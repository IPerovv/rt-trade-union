package com.example.rt.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GroupEvent(
    val id: Int,
    val title: String,
    val description: String,
    val place: String,
    val photo: String,
    val postDate: String, //"2021-09-21T17:00"
    val date: String,
    val time: String,
    val author: String,
    val isVisit: Boolean,
    val isLiked: Boolean,
    val cntLikes: Long,
    val cntComments: Long,
) : Parcelable