package com.example.rt.domain.use_case.validation

import android.util.Patterns

class ValidatePhoneUseCase {
    fun execute(phone: String): ValidationResult {
        if (phone.isBlank()) {
            return ValidationResult(
                successful = false,
                errorMessage = "Заполните номер телефона"
            )
        }

        if (!Patterns.PHONE.matcher(phone).matches()) {
            return ValidationResult(
                successful = false,
                errorMessage = "Это неверный формат телефонного номера!"
            )
        }

        if((phone.length < 11) or (phone.length > 13)) {
            return ValidationResult(
                successful = false,
                errorMessage = "Неверно введенный номер"
            )
        }

        /*if(!((phone[0].toString() == ("+")) and (phone[1].toString() == "7")) or (phone[0].toString() != "8")) {
            return ValidationResult(
                successful = false,
                errorMessage = "Неверно введенный телефонный код"
            )
        }*/

        return ValidationResult(
            successful = true
        )
    }
}