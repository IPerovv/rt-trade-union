package com.example.rt.domain.model

import android.os.Parcelable
import com.example.rt.R
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Document(
    //val datetime: String, //"2021-09-21T17:00"
    val title: String,
    val description: String,
    val subtitle: String = "",
    val photo: Int = R.drawable.image_test_2,//String,
) : Parcelable