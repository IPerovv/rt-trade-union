package com.example.rt.domain.repository

import com.example.rt.data.remote.dto.FileDto
import com.example.rt.utils.Resource
import kotlinx.coroutines.flow.Flow
import okhttp3.MultipartBody

interface PhotoRepository {
    suspend fun uploadPhoto(uri: MultipartBody.Part): FileDto?
}