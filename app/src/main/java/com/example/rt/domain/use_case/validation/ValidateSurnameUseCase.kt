package com.example.rt.domain.use_case.validation

class ValidateSurnameUseCase {
    fun execute(surname: String): ValidationResult {
        if (surname.isBlank()) {
            return ValidationResult(
                successful = false,
                errorMessage = "Введите фамилию"
            )
        }

        if (!surname.matches(Regex("[а-яА-ЯёЁ]+"))) {
            return ValidationResult(
                successful = false,
                errorMessage = "Неверный формат фамилии!"
            )
        }

        if (surname.length < 2) {
            return ValidationResult(
                successful = false,
                errorMessage = "Фамилия слишком короткая!"
            )
        }

        return ValidationResult(
            successful = true
        )
    }
}