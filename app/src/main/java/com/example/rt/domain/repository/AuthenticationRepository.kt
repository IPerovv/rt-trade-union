package com.example.rt.domain.repository

import com.example.rt.data.remote.dto.AuthenticationRequestDto
import com.example.rt.data.remote.dto.AuthenticationResponseDto
import com.example.rt.data.remote.dto.PersonalDataDto
import com.example.rt.utils.Resource
import kotlinx.coroutines.flow.Flow

interface AuthenticationRepository {
    suspend fun authenticate(
        authenticationRequestDto: AuthenticationRequestDto
    ): Flow<AuthenticationResponseDto>

    suspend fun register(
        authenticationRequestDto: AuthenticationRequestDto
    ): Flow<AuthenticationResponseDto>

    suspend fun confirmationCodeFromMail(
        confirmCode: String,
        authenticationRequestDto: AuthenticationRequestDto
    ): Flow<Resource<Boolean>>

    suspend fun updatePersonalData(
        personalDataDto: PersonalDataDto
    ): Flow<Resource<Boolean>>
}