package com.example.rt.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class News(
    val id: Int,
    val date: String, //"2021-09-21T17:00"
    val title: String,
    val description: String,
    val photo: String? = null,
    val cntLikes: Long,
    val cntComments: Long,
    val isLiked: Boolean = false,
) : Parcelable {
    fun doesMatchSearchQuery(query: String): Boolean {
        val matching = listOf(
            title,
            "${title.first()}"
        )

        return matching.any { it.contains(query, ignoreCase = true) }
    }
}