package com.example.rt.domain.model


data class Article(
    val photo: Int,
    val articleName: String,
    val coinsCost: Int,
    //Нужно добавить остальные поля
)