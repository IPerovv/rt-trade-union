package com.example.rt.domain.use_case.validation

import android.util.Patterns

class ValidateEmailUseCase {
    fun execute(email: String): ValidationResult {
        if (email.isBlank()) {
            return ValidationResult(
                successful = false,
                errorMessage = "Почта пустая!"
            )
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            return ValidationResult(
                successful = false,
                errorMessage = "Это неверный формат почты!"
            )
        }

        return ValidationResult(
            successful = true
        )
    }
}