package com.example.rt.domain.repository

import com.example.rt.domain.model.News
import com.example.rt.utils.Resource
import kotlinx.coroutines.flow.Flow

interface NewsRepository {
    fun getNews(
        fetchFromRemote: Boolean
    ): Flow<Resource<List<News>>>

    suspend fun likeNewsById(id: Int): Boolean
    suspend fun unLikeNewsById(id: Int): Boolean
    suspend fun deleteNews(news: News): Boolean
}