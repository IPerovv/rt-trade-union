package com.example.rt.domain.repository

import com.example.rt.domain.model.GroupEvent
import com.example.rt.utils.Resource
import kotlinx.coroutines.flow.Flow

interface GroupEventRepository {
    suspend fun getGroupEvents(
        fetchFromRemote: Boolean,
        pageSize: Int
    ): Flow<Resource<List<GroupEvent>>>

    suspend fun deleteGroupEvent(
        groupEvent: GroupEvent
    ): Boolean
}