package com.example.rt.domain.model


data class User(
    val photo: String?,
    var firstName: String,
    val lastName: String,
    val rating: Long,
)