package com.example.rt.domain.use_case.validation

class ValidateNameUseCase {
    fun execute(name: String): ValidationResult {
        if (name.isBlank()) {
            return ValidationResult(
                successful = false,
                errorMessage = "Введите имя"
            )
        }

        if (!name.matches(Regex("[а-яА-ЯёЁ]+"))) {
            return ValidationResult(
                successful = false,
                errorMessage = "Неверный формат имени!"
            )
        }

        if (name.length < 2) {
            return ValidationResult(
                successful = false,
                errorMessage = "Имя слишком короткое!"
            )
        }

        return ValidationResult(
            successful = true
        )
    }
}