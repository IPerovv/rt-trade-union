package com.example.rt.domain.use_case.validation

import okhttp3.MultipartBody

class ValidatePhotoUseCase {
    fun execute(photo: MultipartBody.Part?): ValidationResult {
        if (photo == null) {
            return ValidationResult(
                successful = false,
                errorMessage = "Добавьте фото"
            )
        }

        return ValidationResult(
            successful = true
        )
    }
}