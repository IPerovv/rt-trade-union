package com.example.rt.domain.use_case.validation

class ValidateBirthDayUseCase {
    fun execute(birthDay: String): ValidationResult {
        if (birthDay.isBlank()) {
            return ValidationResult(
                successful = false,
                errorMessage = "Добавьте дату рождения"
            )
        }

        return ValidationResult(
            successful = true
        )
    }
}