package com.example.rt.domain.use_case.validation

class ValidateLocationUseCase {
    fun execute(location: String): ValidationResult {
        if (location.isBlank()) {
            return ValidationResult(
                successful = false,
                errorMessage = "Введите место проживания"
            )
        }

        if (location.length < 2) {
            return ValidationResult(
                successful = false,
                errorMessage = "Место проживания слишком короткое"
            )
        }

        return ValidationResult(
            successful = true
        )
    }
}