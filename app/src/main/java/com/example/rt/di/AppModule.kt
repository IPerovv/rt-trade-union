package com.example.rt.di

import android.content.Context
import android.content.SharedPreferences
import coil.ImageLoader
import com.example.rt.data.local.AccountStoreManager
import com.example.rt.data.local.RtDataBase
import com.example.rt.data.local.dao.GroupEventDao
import com.example.rt.data.local.dao.NewsDao
import com.example.rt.data.remote.RtApi
import com.example.rt.data.remote.TokenInterceptor
import com.example.rt.data.repository.AuthenticationRepositoryImpl
import com.example.rt.data.repository.GroupEventRepositoryImpl
import com.example.rt.data.repository.NewsRepositoryImpl
import com.example.rt.data.repository.PhotoRepositoryImpl
import com.example.rt.domain.repository.AuthenticationRepository
import com.example.rt.domain.repository.GroupEventRepository
import com.example.rt.domain.repository.NewsRepository
import com.example.rt.domain.repository.PhotoRepository
import com.example.rt.domain.use_case.validation.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {
    @Provides
    fun provideValidateEmailUseCase(): ValidateEmailUseCase {
        return ValidateEmailUseCase()
    }

    @Provides
    fun provideValidatePasswordUseCase(): ValidatePasswordUseCase {
        return ValidatePasswordUseCase()
    }

    @Provides
    fun provideValidateLocatioUseCase(): ValidateLocationUseCase {
        return ValidateLocationUseCase()
    }

    @Provides
    fun provideValidateNameUseCase(): ValidateNameUseCase {
        return ValidateNameUseCase()
    }

    @Provides
    fun provideValidatePhoneUseCase(): ValidatePhoneUseCase {
        return ValidatePhoneUseCase()
    }

    @Provides
    fun provideValidateSurnameUseCase(): ValidateSurnameUseCase {
        return ValidateSurnameUseCase()
    }

    @Provides
    fun provideValidateRepeatedPasswordUseCase(): ValidateRepeatedPasswordUseCase {
        return ValidateRepeatedPasswordUseCase()
    }

    @Provides
    fun provideValidatePhotoUseCase(): ValidatePhotoUseCase {
        return ValidatePhotoUseCase()
    }

    @Provides
    fun provideValidateBirthDayUseCase(): ValidateBirthDayUseCase {
        return ValidateBirthDayUseCase()
    }

    @Singleton
    @Provides
    fun provideGsonConverterFactory(): GsonConverterFactory {
        return GsonConverterFactory.create()
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(interceptor: TokenInterceptor): OkHttpClient {
        return OkHttpClient.Builder()
            .connectTimeout(100, TimeUnit.SECONDS)
            .readTimeout(100, TimeUnit.SECONDS)
            .addInterceptor(interceptor)
            .build()
    }

    @Provides
    @Singleton
    fun provideRtApi(
        okHttpClient: OkHttpClient,
        gsonConverterFactory: GsonConverterFactory
    ): RtApi {
        return Retrofit.Builder()
            .baseUrl("http://31.129.108.71:8080/")
            .client(okHttpClient)
            .addConverterFactory(gsonConverterFactory)
            .build()
            .create(RtApi::class.java)
    }

    @Provides
    @Singleton
    fun provideImageLoader(
        @ApplicationContext context: Context,
        okHttpClient: OkHttpClient
    ): ImageLoader {
        return ImageLoader.Builder(context)
            .okHttpClient(okHttpClient)
            .build()
    }

    @Provides
    @Singleton
    fun provideSharedPreference(@ApplicationContext context: Context): SharedPreferences {
        return context.getSharedPreferences("mySharedPreferences", Context.MODE_PRIVATE)
    }

    @Provides
    @Singleton
    fun provideAccountStoreManager(@ApplicationContext context: Context): AccountStoreManager {
        return AccountStoreManager(context)
    }

    @Provides
    fun provideTokenInterceptor(accountStoreManager: AccountStoreManager): TokenInterceptor {
        return TokenInterceptor(accountStoreManager)
    }

    @Provides
    fun provideAuthenticationRepository(
        accountStoreManager: AccountStoreManager,
        rtApi: RtApi
    ): AuthenticationRepository {
        return AuthenticationRepositoryImpl(accountStoreManager, rtApi)
    }

    @Provides
    fun provideGroupEventRepositoryImpl(
        rtApi: RtApi,
        groupEventDao: GroupEventDao
    ): GroupEventRepository {
        return GroupEventRepositoryImpl(rtApi, groupEventDao)
    }

    @Provides
    fun providePhotoRepository(
        rtApi: RtApi
    ): PhotoRepository {
        return PhotoRepositoryImpl(rtApi)
    }

    @Provides
    fun provideNewsRepository(
        rtApi: RtApi,
        newsDao: NewsDao
    ): NewsRepository {
        return NewsRepositoryImpl(rtApi, newsDao)
    }

    @Provides
    fun provideRtDataBase(@ApplicationContext context: Context): RtDataBase {
        return RtDataBase.getRtDataBaseInstance(context)
    }

    @Provides
    fun provideNewsDao(rtDataBase: RtDataBase): NewsDao {
        return rtDataBase.newsDao()
    }

    @Provides
    fun provideGroupEventDao(rtDataBase: RtDataBase): GroupEventDao {
        return rtDataBase.groupEventDao()
    }

    @Provides
    fun provideDocumentRepository(
        rtApi: RtApi,
        newDao: NewsDao
    ): NewsRepositoryImpl {
        return NewsRepositoryImpl(rtApi, newDao)
    }
}