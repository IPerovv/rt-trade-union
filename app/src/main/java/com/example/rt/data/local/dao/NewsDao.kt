package com.example.rt.data.local.dao

import androidx.room.*
import com.example.rt.data.local.entity.NewsEntity

@Dao
interface NewsDao {
    @Query("SELECT * FROM news ORDER BY id DESC")
    suspend fun getNews(): List<NewsEntity>

    @Query("SELECT * FROM news WHERE id = :id")
    suspend fun getNewsById(id: Int): NewsEntity?

    @Insert(entity = NewsEntity::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertNewsList(news: List<NewsEntity>)

    @Delete(entity = NewsEntity::class)
    suspend fun deleteNews(news: NewsEntity)

    @Query(
        "UPDATE news SET " +
                "likesCount = likesCount + 1," +
                "isLiked = true " +
                "WHERE id = :newsId"
    )
    suspend fun likeNews(newsId: Int)

    @Query(
        "UPDATE news SET " +
                "likesCount = likesCount - 1, " +
                "isLiked = false " +
                "WHERE id = :newsId"
    )
    suspend fun unLikeNews(newsId: Int)

    @Query("DELETE FROM news")
    fun clearNews()
}