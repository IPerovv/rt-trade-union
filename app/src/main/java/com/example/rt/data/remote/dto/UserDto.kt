package com.example.rt.data.remote.dto

import com.google.gson.annotations.SerializedName
import java.util.*

data class UserDto(
    @SerializedName("id")
    val id: Int,
    @SerializedName("firstname")
    val firstname: String,
    @SerializedName("lastname")
    val lastname: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("photo")
    val photo: String,
    @SerializedName("birthdayDate")
    val birthdayDate: Date,
    @SerializedName("phoneNumber")
    val phoneNumber: String,
    @SerializedName("balance")
    val balance: Long,
    @SerializedName("rating")
    val rating: Long,
    @SerializedName("role")
    val role: String
)