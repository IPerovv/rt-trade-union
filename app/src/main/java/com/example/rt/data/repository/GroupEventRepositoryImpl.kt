package com.example.rt.data.repository

import android.util.Log
import com.example.rt.data.local.dao.GroupEventDao
import com.example.rt.data.mapper.toGroupEvent
import com.example.rt.data.mapper.toGroupEventEntity
import com.example.rt.data.remote.RtApi
import com.example.rt.domain.model.GroupEvent
import com.example.rt.domain.repository.GroupEventRepository
import com.example.rt.utils.Resource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GroupEventRepositoryImpl @Inject constructor(
    private val rtApi: RtApi,
    private val groupEventDao: GroupEventDao
) : GroupEventRepository {
    override suspend fun getGroupEvents(
        fetchFromRemote: Boolean,
        pageSize: Int
    ): Flow<Resource<List<GroupEvent>>> {
        return flow {
            emit(Resource.Loading(true))
            val localEvents = groupEventDao.getGroupEvents()

            emit(Resource.Success(
                data = localEvents.map { it.toGroupEvent() }
            ))

            Log.i("local", localEvents.toString())

            val shouldJustLoadOnCache = localEvents.isNotEmpty() && !fetchFromRemote
            if (shouldJustLoadOnCache) {
                emit(Resource.Loading(false))
                return@flow
            }

            try {
                val remoteResponse = rtApi.getGroupEvents(pageNo = 0, pageSize = pageSize)

                if (remoteResponse.isSuccessful) {
                    emit(Resource.Success(
                        data = remoteResponse.body()?.sortedByDescending { it.id }
                            ?.map { it.toGroupEvent() }
                    ))

                    groupEventDao.clearGroupEvents()
                    remoteResponse.body()?.map {
                        it.toGroupEventEntity()
                    }?.let { groupEventDao.insertGroupEventList(it) }
                } else {
                    emit(Resource.Error(message = "Что-то пошло не так, бро"))
                }
            } catch (exception: Exception) {
                Log.e("error", exception.message.toString())
                emit(Resource.Error(message = "Пропал интернет"))
            } finally {
                emit(Resource.Loading(false))
                Log.i("loading", "1")
            }
        }
    }

    override suspend fun deleteGroupEvent(groupEvent: GroupEvent): Boolean {
        return try {
            val result = rtApi.deleteGroupEvent(id = groupEvent.id)
            if (result.isSuccessful) {
                groupEventDao.deleteGroupEvent(groupEvent.toGroupEventEntity())
                true
            } else {
                false
            }
        } catch (exception: Exception) {
            false
        }
    }
}