package com.example.rt.data.local.dao

import androidx.room.*
import com.example.rt.data.local.entity.GroupEventEntity

@Dao
interface GroupEventDao {
    @Query("SELECT * FROM events")
    suspend fun getGroupEvents(): List<GroupEventEntity>

    @Query("SELECT * FROM events WHERE id = :id")
    suspend fun getGroupEventById(id: Int): GroupEventEntity?

    @Insert(entity = GroupEventEntity::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertGroupEventList(news: List<GroupEventEntity>)

    @Delete(entity = GroupEventEntity::class)
    suspend fun deleteGroupEvent(news: GroupEventEntity)

    @Query("DELETE FROM events")
    fun clearGroupEvents()
}