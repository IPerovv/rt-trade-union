package com.example.rt.data.mapper

import com.example.rt.data.remote.dto.AuthenticationResponseDto
import com.google.gson.Gson
import okhttp3.ResponseBody

fun ResponseBody?.toAuthenticationResponseDto(): AuthenticationResponseDto {
    return Gson().fromJson(this?.string(), AuthenticationResponseDto::class.java)
}