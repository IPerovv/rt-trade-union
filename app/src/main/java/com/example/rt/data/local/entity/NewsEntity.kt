package com.example.rt.data.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "news")
data class NewsEntity(
    @PrimaryKey val id: Int? = null,
    val title: String,
    val postedDate: String,
    val description: String,
    val photo: String,
    val likesCount: Long,
    val commentCount: Long,
    val isLiked: Boolean,
)
