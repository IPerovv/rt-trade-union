package com.example.rt.data.remote.dto

import com.google.gson.annotations.SerializedName

data class ApplicationFormDto(
    @SerializedName("topic")
    val topic: String,
    @SerializedName("text")
    val text: String
)
