package com.example.rt.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.rt.data.local.dao.GroupEventDao
import com.example.rt.data.local.dao.NewsDao
import com.example.rt.data.local.entity.GroupEventEntity
import com.example.rt.data.local.entity.NewsEntity

@Database(
    entities = [NewsEntity::class, GroupEventEntity::class],
    version = 3
)
abstract class RtDataBase : RoomDatabase() {
    abstract fun newsDao(): NewsDao
    abstract fun groupEventDao(): GroupEventDao

    companion object {
        private var rtDataBaseInstance: RtDataBase? = null
        fun getRtDataBaseInstance(context: Context): RtDataBase {
            return if (rtDataBaseInstance == null) {
                Room.databaseBuilder(
                    context,
                    RtDataBase::class.java,
                    "QuickChequeDataBase"
                ).fallbackToDestructiveMigration()
                    .allowMainThreadQueries()
                    .build()
            } else {
                rtDataBaseInstance!!
            }
        }
    }
}