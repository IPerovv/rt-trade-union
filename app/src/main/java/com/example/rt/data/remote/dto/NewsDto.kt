package com.example.rt.data.remote.dto

import com.google.gson.annotations.SerializedName

data class NewsDto(
    @SerializedName("id")
    val id: Int? = null,
    @SerializedName("date")
    val date: String? = null,
    @SerializedName("title")
    val title: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("photo")
    val photo: String,
    @SerializedName("amountOfLikes")
    val cntLikes: Long,
    @SerializedName("amountOfComments")
    val cntComments: Long,
    @SerializedName("isLiked")
    val isLiked: Boolean,
)