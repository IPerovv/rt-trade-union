package com.example.rt.data.mapper

import com.example.rt.data.local.entity.GroupEventEntity
import com.example.rt.data.remote.dto.GroupEventDto
import com.example.rt.domain.model.GroupEvent
import java.text.SimpleDateFormat
import java.util.*

fun GroupEventDto.toGroupEvent(): GroupEvent {
    return GroupEvent(
        id = id!!,
        title = title,
        description = description,
        place = place,
        photo = photo,
        postDate = postDate?.let { convertDateToString(it) }.toString(), //"2021-09-21T17:00"
        date = date,
        time = time,
        author = author.toString(),
        isVisit = isVisit!!,
        isLiked = isLiked!!,
        cntLikes = cntLikes!!,
        cntComments = cntComments!!,
    )
}

fun GroupEventDto.toGroupEventEntity(): GroupEventEntity {
    return GroupEventEntity(
        id = id!!,
        title = title,
        description = description,
        place = place,
        photo = photo,
        postDate = postDate?.let { convertDateToString(it) }.toString(), //"2021-09-21T17:00"
        date = date,
        time = time,
        author = author.toString(),
        isVisit = isVisit!!,
        isLiked = isLiked!!,
        cntLikes = cntLikes!!,
        cntComments = cntComments!!,
    )
}

fun GroupEventEntity.toGroupEvent(): GroupEvent {
    return GroupEvent(
        id = id!!,
        title = title,
        description = description,
        place = place,
        photo = photo,
        postDate = postDate, //"2021-09-21T17:00"
        date = date,
        time = time,
        author = author,
        isVisit = isVisit,
        isLiked = isLiked,
        cntLikes = cntLikes,
        cntComments = cntComments,
    )
}

fun GroupEvent.toGroupEventEntity(): GroupEventEntity {
    return GroupEventEntity(
        id = id,
        title = title,
        description = description,
        place = place,
        photo = photo,
        postDate = postDate, //"2021-09-21T17:00"
        date = date,
        time = time,
        author = author,
        isVisit = isVisit,
        isLiked = isLiked,
        cntLikes = cntLikes,
        cntComments = cntComments,
    )
}

private fun convertDateToString(date: Date): String {
    val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss") // Specify the desired date format
    return dateFormat.format(date)
}