package com.example.rt.data.remote

import com.example.rt.data.local.AccountStoreManager
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class TokenInterceptor @Inject constructor(
    private val accountStoreManager: AccountStoreManager
) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request().newBuilder()
        val token = accountStoreManager.getApiKey()
        request.addHeader("Authorization", "Bearer $token")
        return chain.proceed(request.build())
    }
}