package com.example.rt.data.remote.dto

import com.google.gson.annotations.SerializedName
import java.util.*

data class PersonalDataDto(
    @SerializedName("firstname")
    val firstName: String? = null,
    @SerializedName("lastname")
    val lastName: String? = null,
    @SerializedName("email")
    val email: String? = null,
    @SerializedName("password")
    val password: String? = null,
    @SerializedName("photo")
    val photo: String? = null,
    @SerializedName("birthdayDate")
    var birthDay: Date? = null,
    @SerializedName("phoneNumber")
    val phoneNumber: String? = null,
    @SerializedName("location")
    val location: String? = null,
)
