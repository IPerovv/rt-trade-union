package com.example.rt.data.remote.dto

import com.google.gson.annotations.SerializedName

data class LikeDto(
    @SerializedName("id")
    val id: Int,
    @SerializedName("userId")
    val userId: Int
)
