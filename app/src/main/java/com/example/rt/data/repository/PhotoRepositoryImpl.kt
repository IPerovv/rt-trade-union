package com.example.rt.data.repository

import android.util.Log
import com.example.rt.data.remote.RtApi
import com.example.rt.data.remote.dto.FileDto
import com.example.rt.domain.repository.PhotoRepository
import okhttp3.MultipartBody
import javax.inject.Inject

class PhotoRepositoryImpl @Inject constructor(
    private val rtApi: RtApi
) : PhotoRepository {
    override suspend fun uploadPhoto(
        uri: MultipartBody.Part
    ): FileDto? {
        return try {
            val answer = rtApi.uploadPhoto(uri)
            if (answer.isSuccessful) {
                Log.i("photo", answer.body().toString())
                FileDto(answer.body()?.fileName.toString())
            } else {
                Log.e("photo unSuccess", "увы")
                null
            }
        } catch (exception: Exception) {
            Log.e("photo error", exception.message.toString())
            null
        }
    }
}