package com.example.rt.data.mapper

import com.example.rt.data.local.entity.NewsEntity
import com.example.rt.data.remote.dto.NewsDto
import com.example.rt.domain.model.News

fun NewsDto.toNews(): News {
    return News(
        id = id!!,
        date = date.toString(),
        title = title,
        description = description,
        photo = photo,
        cntLikes = cntLikes,
        cntComments = cntComments,
        isLiked = isLiked
    )
}

fun NewsDto.toNewsEntity(): NewsEntity {
    return NewsEntity(
        id = id,
        postedDate = date.toString(),
        title = title,
        description = description,
        photo = photo,
        likesCount = cntLikes,
        commentCount = cntComments,
        isLiked = isLiked
    )
}

fun NewsEntity.toNews(): News {
    return News(
        id = id!!,
        title = title,
        description = description,
        date = postedDate,
        photo = photo,
        cntLikes = likesCount,
        cntComments = commentCount,
        isLiked = isLiked
    )
}

fun News.toNewsEntity(): NewsEntity {
    return NewsEntity(
        title = title,
        description = description,
        postedDate = date,
        photo = photo.toString(),
        likesCount = 0,
        commentCount = 0,
        isLiked = isLiked
    )
}

