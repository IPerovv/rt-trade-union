package com.example.rt.data.repository

import android.util.Log
import com.example.rt.data.local.AccountStoreManager
import com.example.rt.data.mapper.toAuthenticationResponseDto
import com.example.rt.data.remote.RtApi
import com.example.rt.data.remote.dto.AuthenticationRequestDto
import com.example.rt.data.remote.dto.AuthenticationResponseDto
import com.example.rt.data.remote.dto.PersonalDataDto
import com.example.rt.domain.repository.AuthenticationRepository
import com.example.rt.utils.Resource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class AuthenticationRepositoryImpl @Inject constructor(
    private val accountStoreManager: AccountStoreManager,
    private val rtApi: RtApi
) : AuthenticationRepository {
    override suspend fun authenticate(
        authenticationRequestDto: AuthenticationRequestDto
    ): Flow<AuthenticationResponseDto> = flow {
        try {
            Log.i("s", authenticationRequestDto.toString())
            val response = rtApi.authorization(authenticationRequestDto)

            if (response.isSuccessful) {
                Log.i("MyTag", "successful authentication")
                val userInfo = response.body()?.user
                userInfo?.let { user ->
                    accountStoreManager
                        .saveId(user.id)
                        .saveFirstName(user.firstname)
                        .saveLastName(user.lastname)
                        .savePhotoUrl(user.photo)
                        .saveEmail(user.email)
                        .saveBalance(user.balance)
                        .saveRating(user.rating)
                        .saveApiKey(response.body()?.token.toString())
                }

                response.body()?.let { emit(it) }
            } else {
                Log.i("MyTag", "UnSuccessful authentication")
                emit(response.errorBody().toAuthenticationResponseDto())
            }
        } catch (exception: Exception) {
            Log.i("MyTag", "UnSuccessful authentication ${exception.message}")
            return@flow
        }
    }

    override suspend fun register(
        authenticationRequestDto: AuthenticationRequestDto
    ): Flow<AuthenticationResponseDto> = flow {
        try {
            val response = rtApi.register(authenticationRequestDto)
            Log.i("MyTag", response.code().toString())

            if (response.isSuccessful) {
                Log.i("MyTag", "successful registration")
                response.body()?.let { emit(it) }
            } else {
                Log.i("MyTag", "UnSuccessful registration")
                emit(response.errorBody().toAuthenticationResponseDto())
            }
        } catch (exception: Exception) {
            Log.i("MyTag", "UnSuccessful authentication ${exception.message}")
            return@flow
        }
    }

    override suspend fun confirmationCodeFromMail(
        confirmCode: String,
        authenticationRequestDto: AuthenticationRequestDto
    ): Flow<Resource<Boolean>> {
        return flow {
            try {
                Log.i("mytag", confirmCode)
                emit(Resource.Loading(true))

                val response = rtApi.confirmCodeFromMail(
                    confirmCode,
                    authenticationRequestDto
                )

                Log.i("MyTag", response.code().toString())

                if (response.isSuccessful) {
                    val userInfo = response.body()?.user
                    userInfo?.let { user ->
                        accountStoreManager
                            .saveId(user.id)
                            .saveApiKey(response.body()?.token.toString())
                    }
                    Log.i("MyTag", "successful code confirm")
                    emit(Resource.Success(true))
                } else {
                    Log.i("MyTag", "UnSuccessful code confirm")
                    emit(Resource.Success(false))
                }
            } catch (exception: Exception) {
                Log.i("MyTag", "UnSuccessful code confirm ${exception.message}")
                emit(Resource.Error(exception.message))
            } finally {
                emit(Resource.Loading(false))
            }
        }
    }

    override suspend fun updatePersonalData(
        personalDataDto: PersonalDataDto
    ): Flow<Resource<Boolean>> {
        return flow {
            try {
                emit(Resource.Loading(true))
                val myId = accountStoreManager.getId()
                Log.i("MY ID", myId.toString())
                if (myId != 0) {
                    val result = rtApi.updatePersonalData(myId, personalDataDto)
                    if (result.isSuccessful) {
                        val userInfo = result.body()
                        userInfo?.let { user ->
                            accountStoreManager.saveFirstName(user.firstname)
                                .saveLastName(user.lastname)
                                .savePhotoUrl(user.photo)
                                .saveEmail(user.email)
                                .saveBalance(user.balance)
                                .saveRating(user.rating)
                        }
                        emit(Resource.Success(true))
                    } else {
                        emit(Resource.Error("Ахуеть"))
                    }
                }
            } catch (exception: Exception) {
                emit(Resource.Error(exception.message))
            } finally {
                emit(Resource.Loading(false))
            }
        }
    }
}