package com.example.rt.data.repository

import android.util.Log
import com.example.rt.data.local.dao.NewsDao
import com.example.rt.data.mapper.toNews
import com.example.rt.data.mapper.toNewsEntity
import com.example.rt.data.remote.RtApi
import com.example.rt.domain.model.News
import com.example.rt.domain.repository.NewsRepository
import com.example.rt.utils.Resource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class NewsRepositoryImpl @Inject constructor(
    private val rtApi: RtApi,
    private val newsDao: NewsDao,
) : NewsRepository {
    override fun getNews(
        fetchFromRemote: Boolean
    ): Flow<Resource<List<News>>> {
        return flow {
            emit(Resource.Loading(true))
            val localNews = newsDao.getNews()
            emit(Resource.Success(
                data = localNews.map { it.toNews() }
            ))
            Log.i("local", localNews.toString())

            val shouldJustLoadOnCache = localNews.isNotEmpty() && !fetchFromRemote
            if (shouldJustLoadOnCache) {
                emit(Resource.Loading(false))
                return@flow
            }

            try {
                val remoteResponse = rtApi.getNews(pageNo = 0, pageSize = 100)

                if (remoteResponse.isSuccessful) {
                    Log.i("MyTag", remoteResponse.body().toString())
                    emit(Resource.Success(
                        data = remoteResponse.body()?.sortedByDescending { it.id }?.map { it.toNews() }
                    ))

                    newsDao.clearNews()

                    remoteResponse.body()?.map {
                        it.toNewsEntity()
                    }?.let { newsDao.insertNewsList(it) }
                } else {
                    emit(Resource.Error(message = "Что-то пошло не так, бро"))
                }
            } catch (exception: Exception) {
                Log.e("error", exception.message.toString())
                emit(Resource.Error(message = "Пропал интернет"))
            } finally {
                emit(Resource.Loading(false))
                Log.i("loading", "1")
            }
        }
    }

    override suspend fun likeNewsById(id: Int): Boolean {
        return try {
            val result = rtApi.likeNews(id)
            if (result.isSuccessful) {
                newsDao.likeNews(id)
                true
            } else {
                false
            }
        } catch (exception: Exception) {
            false
        }
    }

    override suspend fun unLikeNewsById(id: Int): Boolean {
        return try {
            val result = rtApi.unLikeNews(id)
            if (result.isSuccessful) {
                newsDao.unLikeNews(id)
                true
            } else {
                false
            }
        } catch (exception: Exception) {
            false
        }
    }

    override suspend fun deleteNews(news: News): Boolean {
        return try {
            val result = rtApi.deleteNews(id = news.id)
            if (result.isSuccessful) {
                newsDao.deleteNews(news.toNewsEntity())
                true
            } else {
                false
            }
        } catch (exception: Exception) {
            false
        }
    }
}