package com.example.rt.data.remote.dto

import com.google.gson.annotations.SerializedName
import java.util.*

data class GroupEventDto(
    @SerializedName("id")
    val id: Int? = null,
    @SerializedName("title")
    val title: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("placeName")
    val place: String,
    @SerializedName("photo")
    val photo: String,
    @SerializedName("postDate")
    val postDate: Date? = null,
    @SerializedName("date")
    val date: String,
    @SerializedName("time")
    val time: String,
    @SerializedName("author")
    val author: String? = null,
    @SerializedName("willVisit")
    val isVisit: Boolean? = null,
    @SerializedName("isLiked")
    val isLiked: Boolean? = null,
    @SerializedName("amountOfLikes")
    val cntLikes: Long? = null,
    @SerializedName("amountOfComments")
    val cntComments: Long? = null,
)
