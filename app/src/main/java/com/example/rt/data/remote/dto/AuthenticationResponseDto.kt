package com.example.rt.data.remote.dto

import com.google.gson.annotations.SerializedName

data class AuthenticationResponseDto(
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("token")
    val token: String? = null,
    @SerializedName("user")
    val user: UserDto? = null,
)
