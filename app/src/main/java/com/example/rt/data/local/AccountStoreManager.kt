package com.example.rt.data.local

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.*
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.runBlocking

private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "MyAccount")

class AccountStoreManager(val context: Context) {
    suspend fun saveFirstName(firstName: String): AccountStoreManager {
        context.dataStore.edit { pref ->
            pref[stringPreferencesKey("FIRST_NAME")] = firstName
        }
        return this
    }

    suspend fun saveLastName(lastName: String): AccountStoreManager {
        context.dataStore.edit { pref ->
            pref[stringPreferencesKey("LAST_NAME")] = lastName
        }
        return this
    }

    suspend fun saveEmail(email: String): AccountStoreManager {
        context.dataStore.edit { pref ->
            pref[stringPreferencesKey("EMAIL")] = email
        }
        return this
    }

    suspend fun saveId(id: Int): AccountStoreManager {
        context.dataStore.edit { pref ->
            pref[intPreferencesKey("ID")] = id
        }
        return this
    }

    suspend fun saveBalance(balance: Long): AccountStoreManager {
        context.dataStore.edit { pref ->
            pref[longPreferencesKey("BALANCE")] = balance
        }
        return this
    }

    suspend fun saveRating(rating: Long): AccountStoreManager {
        context.dataStore.edit { pref ->
            pref[longPreferencesKey("RATING")] = rating
        }
        return this
    }

    suspend fun saveApiKey(apiKey: String): AccountStoreManager {
        context.dataStore.edit { pref ->
            pref[stringPreferencesKey("API_KEY")] = apiKey
        }
        return this
    }

    suspend fun savePhotoUrl(photoUrl: String): AccountStoreManager {
        context.dataStore.edit { pref ->
            pref[stringPreferencesKey("PHOTO_URL")] = photoUrl
        }
        return this
    }

    fun getFirstName() = runBlocking {
        context.dataStore.data.map { pref ->
            return@map pref[stringPreferencesKey("FIRST_NAME")] ?: ""
        }.first()
    }

    fun getLastName() = runBlocking {
        context.dataStore.data.map { pref ->
            return@map pref[stringPreferencesKey("LAST_NAME")] ?: ""
        }.first()
    }

    fun getRating() = runBlocking {
        context.dataStore.data.map { pref ->
            return@map pref[longPreferencesKey("RATING")] ?: 0
        }.first()
    }

    fun getBalance() = runBlocking {
        context.dataStore.data.map { pref ->
            return@map pref[longPreferencesKey("BALANCE")] ?: 0
        }.first()
    }

    fun getId() = runBlocking {
        context.dataStore.data.map { pref ->
            return@map pref[intPreferencesKey("ID")] ?: 0
        }.first()
    }

    fun getEmail() = runBlocking {
        context.dataStore.data.map { pref ->
            return@map pref[stringPreferencesKey("EMAIL")] ?: ""
        }.first()
    }

    fun getApiKey() = runBlocking {
        context.dataStore.data.map { pref ->
            return@map pref[stringPreferencesKey("API_KEY")] ?: ""
        }.first()
    }

    fun getPhotoUrl() = runBlocking {
        context.dataStore.data.map { pref ->
            return@map pref[stringPreferencesKey("PHOTO_URL")] ?: ""
        }.first()
    }
}