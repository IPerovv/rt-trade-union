package com.example.rt.data.remote

import com.example.rt.data.remote.dto.*
import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.http.*

interface RtApi {
    @POST("auth/register")
    suspend fun register(
        @Body authenticationRequest: AuthenticationRequestDto
    ): Response<AuthenticationResponseDto>

    @PUT("auth/authenticate")
    suspend fun authorization(
        @Body authenticationRequest: AuthenticationRequestDto
    ): Response<AuthenticationResponseDto>

    @PUT("auth/activate/{confirmCode}")
    suspend fun confirmCodeFromMail(
        @Path("confirmCode") confirmCode: String,
        @Body authenticationRequest: AuthenticationRequestDto
    ): Response<AuthenticationResponseDto>

    @GET("news/full")
    @Headers("Content-Type: application/json")
    suspend fun getNews(
        @Query("pageNo") pageNo: Int,
        @Query("pageSize") pageSize: Int
    ): Response<List<NewsDto>>

    @GET("planned-activities/full")
    @Headers("Content-Type: application/json")
    suspend fun getGroupEvents(
        @Query("pageNo") pageNo: Int,
        @Query("pageSize") pageSize: Int
    ): Response<List<GroupEventDto>>

    @POST("feedback")
    suspend fun postApplicationForm(
        @Body applicationFormDto: ApplicationFormDto
    ): Response<AuthenticationResponseDto>

    @POST("news")
    suspend fun postNews(
        @Body newsDto: NewsDto
    ): Response<NewsDto>

    @DELETE("news/{id}")
    suspend fun deleteNews(
        @Path("id") id: Int
    ): Response<NewsDto>

    @POST("planned-activities/manage/")
    suspend fun postGroupEvents(
        @Body groupEventDto: GroupEventDto
    ): Response<GroupEventDto>

    @DELETE("planned-activities/manage/{id}")
    suspend fun deleteGroupEvent(
        @Path("id") id: Int
    ): Response<GroupEventDto>


    @GET("user/rating/top3")
    @Headers("Content-Type: application/json")
    suspend fun getTopFreeUser(): Response<List<UserDto>>

    @POST("news/{id}/likes")
    suspend fun likeNews(
        @Path("id") id: Int
    ): Response<LikeDto>

    @DELETE("news/{id}/likes")
    suspend fun unLikeNews(
        @Path("id") id: Int
    ): Response<LikeDto>

    @POST("document")
    suspend fun postDocument(
        @Body DocumentDto: DocumentDto
    ): Response<DocumentDto>

    @Multipart
    @POST("files")
    suspend fun uploadPhoto(
        @Part photo: MultipartBody.Part,
    ): Response<FileDto>

    @PUT("user/{id}/info")
    suspend fun updatePersonalData(
        @Path("id") userId: Int,
        @Body personalData: PersonalDataDto
    ): Response<UserDto>
}