package com.example.rt.data.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "events")
data class GroupEventEntity(
    @PrimaryKey val id: Int? = null,
    val title: String,
    val description: String,
    val place: String,
    val photo: String,
    val postDate: String, //"2021-09-21T17:00"
    val date: String,
    val time: String,
    val author: String,
    val isVisit: Boolean,
    val isLiked: Boolean,
    val cntLikes: Long,
    val cntComments: Long,
)