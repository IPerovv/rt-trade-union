package com.example.rt.data.remote.dto

import com.google.gson.annotations.SerializedName

data class FileDto(
    @SerializedName("fileName")
    val fileName: String
)