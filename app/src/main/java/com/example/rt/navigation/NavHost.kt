package com.example.rt.navigation

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.rt.domain.model.Document
import com.example.rt.domain.model.GroupEvent
import com.example.rt.domain.model.News
import com.example.rt.presentation.auth_screens.*
import com.example.rt.presentation.main_menu_screens.*
import com.example.rt.presentation.single_screens.SingleDocumentScreen
import com.example.rt.presentation.single_screens.SingleGroupEventScreen
import com.example.rt.presentation.single_screens.SingleNewsScreen

sealed class NavRoute(val route: String) {
    object AuthScreen : NavRoute("auth_screen")
    object EmailConfirmationScreen : NavRoute("email_confirm_screen")
    object RestoreAccountScreen : NavRoute("restore_account_screen")
    object InputCodeScreen : NavRoute("input_code_screen")
    object InputPersonalDataScreen: NavRoute("input_personal_data_screen")
    object NewsScreen : NavRoute("news_screen")
    object CreateNewsScreen : NavRoute("create_news_screen")
    object EventScreen : NavRoute("event_screen")
    object CreateGroupEventScreen : NavRoute("create_event_screen")
    object DocumentScreen : NavRoute("document_screen")
    object CreateDocumentScreen : NavRoute("create_documents_screen")
    object FeedbackScreen : NavRoute("feedback_screen")
    object ProfileScreen : NavRoute("profile_screen")
    object SingleNewsScreen : NavRoute("single_news_screen")
    object SingleDocumentScreen : NavRoute("single_document_screen")
    object SingleGroupEventScreen : NavRoute("single_group_event_screen")
    object RatingScreen : NavRoute("rating_screen")
    object RTCoinScreen : NavRoute("rt_coin_screen")
}

@RequiresApi(Build.VERSION_CODES.O)
@ExperimentalMaterialApi
@Composable
fun NavHostRT() {
    val navController = rememberNavController()

    NavHost(
        navController = navController,
        startDestination = NavRoute.NewsScreen.route,
    ) {
        composable(NavRoute.AuthScreen.route) {
            AuthScreen(navController)
        }

        composable(NavRoute.EmailConfirmationScreen.route + "/{scenario}") { navBackStackEntry ->
            val scenario = navBackStackEntry.arguments?.getString("scenario")
            EmailConfirmationScreen(navController = navController, scenario = scenario)
        }

        composable(NavRoute.InputCodeScreen.route + "/{scenario}") { navBackStackEntry ->
            val scenario = navBackStackEntry.arguments?.getString("scenario")
            InputCodeScreen(navController, scenario = scenario)
        }

        composable(NavRoute.InputPersonalDataScreen.route) {
            InputPersonalDataScreen(navController = navController)
        }

        composable(NavRoute.RestoreAccountScreen.route) {
            RestoreAccountScreen(navController)
        }

        composable(NavRoute.NewsScreen.route) {
            ListedNewsScreen(navController)
        }

        composable(NavRoute.CreateNewsScreen.route) {
            CreateNewsScreen(navController)
        }

        composable(NavRoute.EventScreen.route) {
            ListedGroupEventsScreen(navController)
        }

        composable(NavRoute.CreateGroupEventScreen.route) {
            CreateGroupEventsScreen(navController)
        }

        composable(NavRoute.DocumentScreen.route) {
            DocumentsScreen(navController)
        }

        composable(NavRoute.CreateDocumentScreen.route) {
            CreateDocumentsScreen(navController)
        }

        composable(NavRoute.ProfileScreen.route) {
            ProfileScreen(navController)
        }

        composable(NavRoute.FeedbackScreen.route) {
            FeedbackScreen(navController)
        }

        composable(NavRoute.RatingScreen.route) {
            RatingScreen(navController)
        }

        composable(NavRoute.RTCoinScreen.route) {
            val balance = navController.previousBackStackEntry?.arguments?.getLong("BALANCE")
            if (balance != null) {
                RTCoinScreen(navController,balance)
            }
        }

        composable(NavRoute.SingleNewsScreen.route) {
            val news = navController.previousBackStackEntry?.arguments?.getParcelable<News>("NEWS")
            if (news != null) {
                SingleNewsScreen(news, navController)
            }
        }

        composable(NavRoute.SingleDocumentScreen.route) {
            val document =
                navController.previousBackStackEntry?.arguments?.getParcelable<Document>("DOCUMENT")
            if (document != null) {
                SingleDocumentScreen(document, navController)
            }
        }

        composable(NavRoute.SingleGroupEventScreen.route) {
            val groupEvent =
                navController.previousBackStackEntry?.arguments?.getParcelable<GroupEvent>("GROUP EVENT")
            if (groupEvent != null) {
                SingleGroupEventScreen(groupEvent, navController)
            }
        }
    }
}
