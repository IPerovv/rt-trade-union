package com.example.rt.navigation.nav_item

import com.example.rt.R

sealed class BottomNavItem(
    val title: String,
    val iconId: Int,
    val route: String
) {
    object NewsScreen : BottomNavItem(
        title = "Новости",
        iconId = R.drawable.icon_newspaper,
        route = "news_screen"
    )

    object EventScreen : BottomNavItem(
        title = "События",
        iconId = R.drawable.icon_event,
        route = "event_screen"
    )

    object DocumentScreen : BottomNavItem(
        title = "Документы",
        iconId = R.drawable.icon_document,
        route = "document_screen"
    )

    object ProfileScreen : BottomNavItem(
        title = "Профиль",
        iconId = R.drawable.icon_person,
        route = "profile_screen"
    )
}