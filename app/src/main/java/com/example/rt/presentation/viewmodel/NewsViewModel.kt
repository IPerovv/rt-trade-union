package com.example.rt.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import coil.ImageLoader
import com.example.rt.domain.model.News
import com.example.rt.domain.repository.NewsRepository
import com.example.rt.presentation.event.NewsEvent
import com.example.rt.presentation.event.ValidationEvent
import com.example.rt.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NewsViewModel @Inject constructor(
    private val newsRepository: NewsRepository,
    private val imageLoader: ImageLoader
) : ViewModel() {
    private val _validationEventChannel = Channel<ValidationEvent>()
    val validationEventChannel = _validationEventChannel.receiveAsFlow()

    data class NewsItemState(
        var listNews: List<News> = emptyList(),
        var isLoading: Boolean = false,
    )

    private var _state = MutableStateFlow(NewsItemState())
    val state = _state.asStateFlow()

    init {
        getNews()
    }

    fun onEvent(newsEvent: NewsEvent) {
        when (newsEvent) {
            is NewsEvent.Refresh -> {
                getNews()
            }
            is NewsEvent.DeleteNews -> {
                viewModelScope.launch {
                    val result = newsRepository.deleteNews(news = newsEvent.news)
                    _validationEventChannel.send(
                        if (result) {
                            ValidationEvent.Success
                        } else {
                            ValidationEvent.Failure
                        }
                    )
                }
            }
            is NewsEvent.Like -> {
                viewModelScope.launch {
                    newsRepository.likeNewsById(newsEvent.newsId)
                }
            }
            is NewsEvent.UnLike -> {
                viewModelScope.launch {
                    newsRepository.unLikeNewsById(newsEvent.newsId)
                }
            }
        }
    }

    private fun getNews() {
        viewModelScope.launch {
            newsRepository.getNews(true).collect { news ->
                when (news) {
                    is Resource.Loading -> {
                        _state.value = _state.value.copy(isLoading = news.isLoading)
                        Log.i("ds", state.toString())
                    }
                    is Resource.Error -> {
                        Log.e("error", news.message.toString())
                    }
                    is Resource.Success -> {
                        _state.value = _state.value.copy(listNews = news.data!!)
                    }
                }
            }
        }
    }

    fun getLoadingState(): Boolean = _state.value.isLoading
    fun getImageLoader(): ImageLoader = imageLoader
}