package com.example.rt.presentation.event

sealed class FeedBackEvent {
    data class TextChanges(val text: String): FeedBackEvent()
    object SubmitData: FeedBackEvent()
}
