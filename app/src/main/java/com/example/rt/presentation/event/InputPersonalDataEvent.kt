package com.example.rt.presentation.event

import okhttp3.MultipartBody

sealed class InputPersonalDataEvent {
    data class PhotoOnChanged(val photoPart: MultipartBody.Part) : InputPersonalDataEvent()
    data class FirstNameOnChanged(val firstName: String) : InputPersonalDataEvent()
    data class LastNameOnChanged(val lastName: String) : InputPersonalDataEvent()
    data class BirthDayOnChanged(val birthDay: String) : InputPersonalDataEvent()
    data class PhoneNumberOnChanged(val phoneNumber: String) : InputPersonalDataEvent()
    data class LocationOnChanged(val location: String) : InputPersonalDataEvent()
    object Submit : InputPersonalDataEvent()
}
