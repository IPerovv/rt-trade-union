package com.example.rt.presentation.page_elements.content_parts

import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.example.rt.presentation.theme.customAppTypography

@Composable
fun ContentTitle(title: String) {
    Text(
        //Заголовок
        text = title,
        style = customAppTypography.caption,
        modifier = Modifier.padding(horizontal = 10.dp),
    )
}