package com.example.rt.presentation.viewmodel

import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.rt.data.local.AccountStoreManager
import com.example.rt.data.remote.dto.AuthenticationRequestDto
import com.example.rt.domain.repository.AuthenticationRepository
import com.example.rt.domain.use_case.validation.ValidateEmailUseCase
import com.example.rt.domain.use_case.validation.ValidatePasswordUseCase
import com.example.rt.domain.use_case.validation.ValidateRepeatedPasswordUseCase
import com.example.rt.presentation.event.AuthFormEvent
import com.example.rt.presentation.event.AuthorizationState
import com.example.rt.presentation.event.ValidationEvent
import com.example.rt.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject


@HiltViewModel
class AuthViewModel @Inject constructor(
    private val accountStoreManager: AccountStoreManager,
    private val authenticationRepository: AuthenticationRepository,
    private val validateEmail: ValidateEmailUseCase,
    private val validatePassword: ValidatePasswordUseCase,
    private val validateRepeatedPassword: ValidateRepeatedPasswordUseCase,
) : ViewModel() {
    private val _validationEventChannel = Channel<ValidationEvent>()
    val validationEventChannel = _validationEventChannel.receiveAsFlow()

    private val _authValidationEventChannel = Channel<ValidationEvent>()
    val authValidationEventChannel = _authValidationEventChannel.receiveAsFlow()

    var state by mutableStateOf(AuthorizationState())

    fun onEvent(event: AuthFormEvent) {
        when (event) {
            is AuthFormEvent.EmailOnChanged -> {
                state = state.copy(email = event.email)
            }
            is AuthFormEvent.PasswordOnChanged -> {
                state = state.copy(password = event.password)
            }
            is AuthFormEvent.RepeatedPasswordOnChanged -> {
                state = state.copy(repeatedPassword = event.repeatedPassword)
            }
            is AuthFormEvent.ConfirmCodeOnChanged -> {
                Log.i("letter", event.index.toString())
                Log.i("letter", event.letter.toString())

                val confirmCode = state.confirmCode?.toMutableList() ?: mutableListOf()
                confirmCode[event.index] = if (event.letter.toString().isEmpty()) {
                    ' '
                } else {
                    event.letter
                }

                state = state.copy(confirmCode = confirmCode)
            }
            is AuthFormEvent.RegistrationSubmit -> {
                registrationSubmit()
            }
            is AuthFormEvent.AuthorizationSubmit -> {
                authorizationSubmit()
            }
            is AuthFormEvent.ConfirmCodeSubmit -> {
                confirmCodeSubmit()
            }
        }
    }

    private suspend fun authorization(): Boolean {
        accountStoreManager.saveApiKey("")
        val result = authenticationRepository.authenticate(
            AuthenticationRequestDto(
                email = state.email.lowercase(),
                password = state.password,
            )
        ).firstOrNull()

        if (result?.status == "FAILURE" || result == null) {
//            state = state.copy(
//                emailError = result?.message
//            )
            return false
        }

        return true
    }

    private fun authorizationSubmit() {
        viewModelScope.launch {
            val success = withContext(Dispatchers.IO) {
                authorization()
            }

            _authValidationEventChannel.send(
                if (success) {
                    ValidationEvent.Success
                } else {
                    ValidationEvent.Failure
                }
            )
        }
    }


    private suspend fun register(): Boolean {
        accountStoreManager.saveApiKey("")

        val result = authenticationRepository.register(
            AuthenticationRequestDto(
                email = state.email.lowercase(),
                password = state.password,
            )
        ).firstOrNull()

        if (result?.status == "FAILURE" || result == null) {
            state = state.copy(
                emailError = result?.message
            )
            return false
        }

        return true
    }

    private fun registrationSubmit() {
        viewModelScope.launch {
            val email = validateEmail.execute(state.email)
            val password = validatePassword.execute(state.password)
            val repeatedPassword = validateRepeatedPassword.execute(
                state.password,
                state.repeatedPassword
            )

            val hasError = listOf(email, password, repeatedPassword).any { !it.successful }

            state = state.copy(
                emailError = email.errorMessage,
                passwordError = password.errorMessage,
                repeatedPasswordError = repeatedPassword.errorMessage
            )

            if (!hasError) {
                val success = withContext(Dispatchers.IO) {
                    register()
                }

                _validationEventChannel.send(
                    if (success) {
                        accountStoreManager.saveEmail(state.email.lowercase())
                        ValidationEvent.Success
                    } else {
                        ValidationEvent.Failure
                    }
                )
            }
        }
    }

    private fun confirmCodeSubmit() {
        viewModelScope.launch {
            val email = accountStoreManager.getEmail()

            authenticationRepository.confirmationCodeFromMail(
                state.confirmCode!!.joinToString(""),
                AuthenticationRequestDto(email = email)
            ).collect { result ->
                when (result) {
                    is Resource.Loading -> {

                    }
                    is Resource.Success -> {
                        _validationEventChannel.send(
                            if (result.data == true) {
                                ValidationEvent.Success
                            } else {
                                ValidationEvent.Failure
                            }
                        )
                    }
                    is Resource.Error -> {
                        _validationEventChannel.send(ValidationEvent.Failure)
                    }
                }
            }
        }
    }
}

