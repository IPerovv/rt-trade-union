package com.example.rt.presentation.viewmodel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import coil.ImageLoader
import com.example.rt.data.local.AccountStoreManager
import com.example.rt.data.remote.RtApi
import com.example.rt.domain.model.GroupEvent
import com.example.rt.domain.model.User
import com.example.rt.domain.repository.GroupEventRepository
import com.example.rt.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val accountStoreManager: AccountStoreManager,
    private val imageLoader: ImageLoader,
    private val eventRepository: GroupEventRepository,
    private val rtApi: RtApi
) : ViewModel() {
    data class ProfileState(
        val threeTopUser: List<User> = emptyList(),
        val fiveGroupEvents: List<GroupEvent> = emptyList(),
    )

    var state by mutableStateOf(ProfileState())

    init {
        getTopThreeUser()
        getFiveGroupEvents()
    }

    private fun getTopThreeUser() {
        viewModelScope.launch {
            try {
                val result = rtApi.getTopFreeUser()

                if (result.isSuccessful) {
                    state = state.copy(threeTopUser = result.body()?.map { user ->
                        User(
                            photo = user.photo,
                            firstName = user.firstname,
                            lastName = user.lastname,
                            rating = user.rating
                        )
                    } ?: emptyList())
                }

            } catch (exception: Exception) { }
        }
    }

    private fun getFiveGroupEvents() {
        viewModelScope.launch {
            eventRepository.getGroupEvents(true, 5).collect { event ->
                when (event) {
                    is Resource.Loading -> {}
                    is Resource.Success -> {
                        state = state.copy(fiveGroupEvents = event.data!!)
                    }
                    is Resource.Error -> {}
                }
            }
        }
    }

    fun getImageLoader(): ImageLoader = imageLoader
    fun getAccountManager(): AccountStoreManager = accountStoreManager
}