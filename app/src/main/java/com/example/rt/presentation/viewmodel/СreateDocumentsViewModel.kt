package com.example.rt.presentation.viewmodel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.rt.data.remote.RtApi
import com.example.rt.data.remote.dto.DocumentDto
import com.example.rt.presentation.event.CreateDocumentEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CreateDocumentsViewModel @Inject constructor(
    private val rtApi: RtApi
) : ViewModel() {
    data class CreateDocumentState(
        val title: String = "",
        val description: String = "",
        val photo: String = "",
        val file: String = ""
    )


    var state by mutableStateOf(CreateDocumentState())

    fun onEvent(createDocumentEvent: CreateDocumentEvent) {
        when (createDocumentEvent) {
            is CreateDocumentEvent.TitleChanges -> {
                state = state.copy(title = createDocumentEvent.title)
            }
            is CreateDocumentEvent.DescriptionChanges -> {
                state = state.copy(description = createDocumentEvent.description)
            }
            is CreateDocumentEvent.PhotoChanges -> {
                state = state.copy(photo = createDocumentEvent.photo)
            }
            is CreateDocumentEvent.FileChanges -> {
                state = state.copy(file = createDocumentEvent.file)
            }
            is CreateDocumentEvent.SubmitDocument -> {
                submitDocument()
            }
        }
    }

    private fun submitDocument() {
        viewModelScope.launch {
            rtApi.postDocument(
                DocumentDto(
                    title = state.title,
                    description = state.description,
                    photo = 228,
                    file = ""
                )
            )
        }
    }
}