package com.example.rt.presentation.event

sealed class CreateDocumentEvent {
    data class TitleChanges(val title: String): CreateDocumentEvent()
    data class DescriptionChanges(val description: String): CreateDocumentEvent()
    data class PhotoChanges(val photo: String): CreateDocumentEvent()
    data class FileChanges(val file: String): CreateDocumentEvent()
    object SubmitDocument: CreateDocumentEvent()
}
