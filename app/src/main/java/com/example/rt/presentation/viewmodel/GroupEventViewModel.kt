package com.example.rt.presentation.viewmodel

import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.rt.domain.model.GroupEvent
import com.example.rt.domain.repository.GroupEventRepository
import com.example.rt.presentation.event.ActivityEvent
import com.example.rt.presentation.event.ValidationEvent
import com.example.rt.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class GroupEventViewModel @Inject constructor(
    private val groupEventRepository: GroupEventRepository
) : ViewModel() {
    private val _validationEventChannel = Channel<ValidationEvent>()
    val validationEventChannel = _validationEventChannel.receiveAsFlow()

    data class GroupEventItemState(
        var listGroupEvents: List<GroupEvent> = emptyList(),
        var isLoading: Boolean = false,
    )

    var state by mutableStateOf(GroupEventItemState())

    init {
        getGroupEvents()
    }

    fun onEvent(groupEvent: ActivityEvent) {
        when (groupEvent) {
            is ActivityEvent.Refresh -> {
                getGroupEvents()
            }
            is ActivityEvent.DeleteGroupEvent -> {
                viewModelScope.launch {
                    val result = groupEventRepository.deleteGroupEvent(
                        groupEvent = groupEvent.event
                    )
                    _validationEventChannel.send(
                        if (result) {
                            ValidationEvent.Success
                        } else {
                            ValidationEvent.Failure
                        }
                    )
                }
            }
        }
    }

    private fun getGroupEvents() {
        viewModelScope.launch {
            groupEventRepository.getGroupEvents(true, 100).collect { groupEvent ->
                when (groupEvent) {
                    is Resource.Loading -> {
                        state = state.copy(isLoading = groupEvent.isLoading)
                        Log.i("ds", state.toString())
                    }
                    is Resource.Error -> {
                        Log.e("error", groupEvent.message.toString())
                    }
                    is Resource.Success -> {
                        state = state.copy(listGroupEvents = groupEvent.data!!)
                    }
                }
            }
        }
    }
}