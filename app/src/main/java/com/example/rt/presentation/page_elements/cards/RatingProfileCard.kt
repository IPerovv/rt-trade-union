package com.example.rt.presentation.page_elements.cards

import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import coil.compose.rememberImagePainter
import coil.request.CachePolicy
import coil.request.ImageRequest
import com.example.rt.R
import com.example.rt.domain.model.User
import com.example.rt.navigation.NavRoute
import com.example.rt.presentation.theme.BlueLinkColor
import com.example.rt.presentation.theme.customAppTypography
import com.example.rt.presentation.theme.rostelecomFontFamily
import com.example.rt.presentation.viewmodel.ProfileViewModel

@Composable
fun RatingProfileCard(
    viewModel: ProfileViewModel,
    homeUser: User,
    topUsers: List<User>,
    navController: NavController? = null
) {
    Card(
        //Карточка на которой расположена блок рейтинга
        shape = RoundedCornerShape(14.dp),
        elevation = 5.dp,
        modifier = Modifier
            .fillMaxWidth()
            .padding(5.dp)
            .clip(RoundedCornerShape(14.dp))
            .clickable {
                navController?.navigate(NavRoute.RatingScreen.route)
            },
    ) {
        Column(Modifier.fillMaxWidth()) {

            Spacer(modifier = Modifier.size(7.dp))

            TextRating(homeUser.rating)

            Spacer(modifier = Modifier.size(10.dp))

            Box {
                BackgroundRectangles()
                TopUserByRating(
                    viewModel,
                    homeUser,
                    topUsers
                )
            }

            Spacer(modifier = Modifier.size(7.dp))

            HowToGetRate()

            Spacer(modifier = Modifier.size(7.dp))
        }
    }
}

@Composable
private fun TextRating(rating: Long) {
    Row(
        Modifier
            .fillMaxWidth()
            .padding(horizontal = 15.dp),
        horizontalArrangement = Arrangement.SpaceBetween,
    ) {
        Text(text = "Рейтинг", style = customAppTypography.caption)
        Text(
            text = "$rating очк",
            style = customAppTypography.caption
        )
    }
}

@Composable
private fun BackgroundRectangles() {
    Row(
        Modifier
            .fillMaxWidth(),
        /*.padding(horizontal = 5.dp)*/
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.Bottom,
    ) {
        Image(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 15.dp),
            contentScale = ContentScale.FillWidth,
            painter = painterResource(id = R.drawable.icon_background_rectangles_rating),
            contentDescription = "icon_rating_rectangles",
        )
    }
}


@Composable
private fun TopUserByRating(
    vieModel: ProfileViewModel,
    homeUser: User,
    topUsers: List<User>
) {
    Row(
        Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.Top,
    ) {
        topUsers.forEachIndexed { index, user ->
            when (index) {
                0 -> {
                    PhotoAndName(vieModel, user, 0)
                    Spacer(modifier = Modifier.size(14.dp))
                }

                1 -> {
                    PhotoAndName(vieModel, user, 10)
                    Spacer(modifier = Modifier.size(14.dp))
                }

                2 -> {
                    Spacer(modifier = Modifier.size(14.dp))
                    PhotoAndName(vieModel, user, 15)
                }
            }
        }

        Spacer(modifier = Modifier.size(40.dp))
        myPhotoAndName(vieModel, homeUser, 30)
    }
}

@Composable
private fun myPhotoAndName(
    vieModel: ProfileViewModel,
    user: User,
    spacerDp: Int
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Spacer(modifier = Modifier.size(spacerDp.dp))
        Text(
            text = "Я",
            fontFamily = rostelecomFontFamily,
            fontWeight = FontWeight.Normal,
            fontSize = 14.sp,
        )
        Spacer(modifier = Modifier.height(1.dp))
        TopUserPhoto(vieModel, user.photo)
        Spacer(modifier = Modifier.height(1.dp))
        Text(
            text = "${user.rating} очк",
            fontFamily = rostelecomFontFamily,
            fontWeight = FontWeight.Normal,
            fontSize = 12.sp,
        )
    }
}

@Composable
private fun PhotoAndName(
    vieModel: ProfileViewModel,
    user: User,
    spacerDp: Int
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Spacer(modifier = Modifier.size(spacerDp.dp))
        Text(
            text = user.firstName,
            fontFamily = rostelecomFontFamily,
            fontWeight = FontWeight.Normal,
            fontSize = 14.sp,
        )
        Spacer(modifier = Modifier.height(1.dp))
        TopUserPhoto(vieModel, user.photo)
        Spacer(modifier = Modifier.height(1.dp))
        Text(
            text = "${user.rating} очк",
            fontFamily = rostelecomFontFamily,
            fontWeight = FontWeight.Normal,
            fontSize = 12.sp,
        )
    }
}

@Composable
private fun TopUserPhoto(
    viewModel: ProfileViewModel,
    photoUrl: String?
) {
    val painter = rememberImagePainter(
        request = ImageRequest.Builder(LocalContext.current)
            .data("http://45.9.40.240:8080/files/$photoUrl")
            .memoryCachePolicy(CachePolicy.ENABLED)
            .diskCachePolicy(CachePolicy.ENABLED)
            .networkCachePolicy(CachePolicy.ENABLED)
            .build(),
        imageLoader = viewModel.getImageLoader()
    )

    Image(
        painter = painter,
        contentDescription = "Top user by rating photo",
        contentScale = ContentScale.Crop,
        modifier = Modifier
            .height(75.dp)
            .aspectRatio(3f / 4f)
            .clip(RoundedCornerShape(11.dp))
    )
}


@Composable
private fun HowToGetRate() {
    val swappableStr = remember {
        mutableStateOf("Как получить?")
    }

    Text(
        text = swappableStr.value,
        fontFamily = rostelecomFontFamily,
        fontWeight = FontWeight.Medium,
        fontSize = 12.sp,
        color = BlueLinkColor,
        modifier = Modifier
            .padding(horizontal = 15.dp)
            .animateContentSize()
            .clickable {
                swappableStr.value =
                    "* Рейтинг можно получить за сканирование qr кодов на мероприятиях," +
                            " за участие в конкурсах, за достижения новых высот на работе."
            }
    )
}
