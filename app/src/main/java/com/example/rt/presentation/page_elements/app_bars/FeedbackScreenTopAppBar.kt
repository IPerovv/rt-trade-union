package com.example.rt.presentation.page_elements.app_bars

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.rt.R
import com.example.rt.presentation.theme.rostelecomFontFamily

@Composable
fun FeedbackScreenTopAppBar(
    title: String,
    isVisibleBackButton: Boolean = true,
    navController: NavController? = null,
    onItemClick: () -> Unit
) {
    TopAppBar(
        backgroundColor = Color.White,
        elevation = 5.dp,
        content = {
            Row(
                modifier = Modifier
                    .fillMaxSize(),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically
            ) {
                if (isVisibleBackButton) {
                    Button(
                        modifier = Modifier
                            .fillMaxHeight(),
                        onClick = { navController?.popBackStack() },
                        colors = ButtonDefaults.buttonColors(
                            backgroundColor = Color.White,
                            contentColor = Color.White,
                            disabledContentColor = Color.White,
                            disabledBackgroundColor = Color.White,
                        ),
                        elevation = null
                    ) {
                        Image(
                            painter = painterResource(id = R.drawable.icon_back),
                            contentDescription = "Button with image",
                            contentScale = ContentScale.Fit
                        )
                    }
                }

                Text(
                    text = title,
                    color = Color.Black,
                    style = TextStyle(
                        fontFamily = rostelecomFontFamily,
                        fontWeight = FontWeight.Medium,
                        fontSize = 23.sp
                    )
                )
                Spacer(Modifier.weight(1f, true))

                IconButton(onClick = {

                }) {
                    Icon(
                        painter = painterResource(id = R.drawable.attach_file),
                        tint = Color.Black,
                        contentDescription = "Прикрепить файл"
                    )
                }

                IconButton(
                    onClick = onItemClick
                ) {
                    Icon(
                        painter = painterResource(id = R.drawable.icon_send),
                        tint = Color.Black,
                        contentDescription = "Отправить"
                    )
                }
            }
        }
    )
}