package com.example.rt.presentation.page_elements.cards

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.example.rt.R
import com.example.rt.presentation.page_elements.content_parts.ContentPictureOld
import com.example.rt.presentation.theme.CoinType
import com.example.rt.presentation.theme.customAppTypography

@Composable
fun RTCoinCard(
    balance: Long,
    coinsCost: Int,
    articleName: String,
    picture: Int /*= R.drawable.image_test_3*/
) {

    Card( //Карточка на которой расположен пост с заголовком.
        modifier = Modifier
            .fillMaxWidth()
            .padding(5.dp)
            .clip(RoundedCornerShape(14.dp))
        //.clickable(onClick = onClick),
    ) {
        Box {
            Column {
                Spacer(modifier = Modifier.size(7.dp))
                ContentPictureOld(picture)                  //Фото
                Spacer(modifier = Modifier.size(8.dp))
                ArticleName(articleName = articleName)      //Название товара
                Spacer(modifier = Modifier.size(3.dp))
                HaveCostRow(balance, coinsCost)             //Коины
                Spacer(modifier = Modifier.size(10.dp))
            }
        }
    }
}

@Composable
private fun HaveCostRow(balance: Long, coinsCost: Int) {
    Row(
        Modifier.padding(start = 20.dp),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ) {
        InfoRow(balance, coinsCost)
        Spacer(modifier = Modifier.size(10.dp))
        Image(
            painter = painterResource(id = R.drawable.rt_coin),
            contentDescription = "Rt Coin image",
            Modifier.size(25.dp)
        )
    }
}

@Composable
private fun RtCoinsHave(balance: Long) {
    Text(
        text = balance.toString(),
        style = MaterialTheme.typography.CoinType
    )
}

@Composable
private fun RtCoinsCost(coinsCost: Int) {
    Text(
        text = coinsCost.toString(),
        style = MaterialTheme.typography.CoinType
    )
}

@Composable
private fun InfoRow(balance: Long, coinsCost: Int) {
    RtCoinsHave(balance)
    Spacer(modifier = Modifier.size(3.dp))
    Text(
        text = "/",
        style = MaterialTheme.typography.CoinType
    )
    Spacer(modifier = Modifier.size(3.dp))
    RtCoinsCost(coinsCost)
}

@Composable
private fun ArticleName(articleName: String) {
    Text(
        text = articleName,
        style = customAppTypography.caption,
        modifier = Modifier.padding(start = 20.dp)
    )
}


