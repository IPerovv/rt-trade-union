package com.example.rt.presentation.main_menu_screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Card
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.example.rt.presentation.event.CreateDocumentEvent
import com.example.rt.presentation.page_elements.app_bars.SingleScreenTopBar
import com.example.rt.presentation.page_elements.buttons.ClassicButton
import com.example.rt.presentation.page_elements.content_parts.InputPhotoCreatePost
import com.example.rt.presentation.page_elements.content_parts.TextFieldCreatePost
import com.example.rt.presentation.theme.GrayBackgroundColor
import com.example.rt.presentation.theme.customAppTypography
import com.example.rt.presentation.viewmodel.CreateDocumentsViewModel

@Composable
fun CreateDocumentsScreen(
    navController: NavController,
    createDocumentsViewModel: CreateDocumentsViewModel = hiltViewModel()
) {
    val state = createDocumentsViewModel.state
    Scaffold(
        modifier = Modifier.fillMaxSize(),
        content = {
            it
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .background(color = GrayBackgroundColor),
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                SingleScreenTopBar(
                    onClick = {},
                    text = "Создание документов",
                    navController = navController,
                    dotsBool = false
                )

                Card( //Карточка на которой расположен пост
                    shape = RoundedCornerShape(14.dp),
                    elevation = 5.dp,
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(top = 6.dp)
                        .verticalScroll(rememberScrollState())
                ) {
                    Column(
                        Modifier.fillMaxSize(),
                        verticalArrangement = Arrangement.SpaceBetween
                    ) {
                        Column {

                            CustomSpacer()

                            InputPhotoCreatePost(onClick = {
                                //getContent.launch("image/*")
                            })

                            CustomSpacer()

                            TextFieldCreatePost(
                                state = state.title,
                                text = "Название",
                                onValueChange = { newText ->
                                    createDocumentsViewModel.onEvent(
                                        CreateDocumentEvent.TitleChanges(
                                            newText.toString()
                                        )
                                    )
                                }
                            )

                            CustomSpacer()

                            TextFieldCreatePost(
                                state = state.description,
                                text = "Текст",
                                onValueChange = { newText ->
                                    createDocumentsViewModel.onEvent(
                                        CreateDocumentEvent.DescriptionChanges(
                                            newText.toString()
                                        )
                                    )
                                }
                            )

                            CustomSpacer()

                            TextFieldCreatePost(
                                state = state.file,
                                text = "+ Загрузить файл",
                                onValueChange = { newText ->
                                    createDocumentsViewModel.onEvent(
                                        CreateDocumentEvent.FileChanges(
                                            newText as String
                                        )
                                    )
                                }
                            )

                        }
                        ClassicButton(
                            "Опубликовать",
                            Modifier
                                .fillMaxWidth()
                                .padding(start = 35.dp, end = 35.dp, bottom = 20.dp)
                                .height(45.dp),
                            onClick = {
                                navController.popBackStack()
                                createDocumentsViewModel.onEvent(CreateDocumentEvent.SubmitDocument)
                            }
                        )
                    }
                }
            }
        }
    )
}


@Composable
private fun AdditionalSection() {
    Text(
        //Заголовок
        text = "Дополнительно:",
        style = customAppTypography.subtitle1,
        modifier = Modifier.padding(horizontal = 10.dp),
    )
}

@Composable
private fun CustomSpacer() {
    Spacer(
        modifier = Modifier
            .height(25.dp)
            .fillMaxWidth()
            .background(color = Color.White)
    )
}
