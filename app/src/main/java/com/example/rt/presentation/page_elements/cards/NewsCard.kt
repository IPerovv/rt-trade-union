package com.example.rt.presentation.page_elements.cards

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.unit.dp
import com.example.rt.domain.model.News
import com.example.rt.presentation.page_elements.buttons.CommentButton
import com.example.rt.presentation.page_elements.buttons.LikeButton
import com.example.rt.presentation.page_elements.content_parts.ContentPicture
import com.example.rt.presentation.page_elements.content_parts.ContentPostedDate
import com.example.rt.presentation.page_elements.content_parts.ContentTitle
import com.example.rt.presentation.viewmodel.NewsViewModel


@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun NewsCard(
    viewModel: NewsViewModel,
    item: News,
    onClick: () -> Unit,
) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(5.dp)
            .clip(RoundedCornerShape(14.dp))
            .clickable(onClick = onClick),
        shape = RoundedCornerShape(14.dp),
        elevation = 5.dp,
    ) {
        Box {
            Column {
                Spacer(modifier = Modifier.size(7.dp))
                ContentPostedDate(item.date)                  //Дата
                Spacer(modifier = Modifier.size(3.dp))
                ContentPicture(
                    viewModel = viewModel,
                    photoUrl = item.photo
                )       //Фото
                Spacer(modifier = Modifier.size(8.dp))
                ContentTitle(item.title)                      //Заголовок
                Spacer(modifier = Modifier.size(20.dp))
                ButtonRow(viewModel = viewModel, item = item) //Строка с кнопками
                Spacer(modifier = Modifier.size(10.dp))
            }
        }
    }
}

@Composable
private fun ButtonRow(
    viewModel: NewsViewModel,
    item: News
) {
    Row(
        //Нижний ряд под кнопки ThumbUp, Comment
        modifier = Modifier
            .padding(horizontal = 10.dp)
            .fillMaxWidth(),
        horizontalArrangement = Arrangement.Start,
    ) {
        LikeButton(
            viewModel = viewModel,
            newsId = item.id,
            likeBool = item.isLiked,
            likesCount = item.cntLikes
        )
        Spacer(Modifier.width(10.0.dp))
        CommentButton(item.cntComments)
    }
}



