package com.example.rt.presentation.page_elements.app_bars

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.rt.R
import com.example.rt.presentation.theme.rostelecomFontFamily


@Composable
fun SingleScreenTopBar(
    onClick: () -> Unit,
    text: String,
    navController: NavController,
    dotsBool: Boolean = true,
    QRBool: Boolean = false
) {
    var expandedDots by remember { mutableStateOf(false) }

    TopAppBar(
        backgroundColor = Color.White,
        elevation = 5.dp,
        content = {
            Row(
                modifier = Modifier.fillMaxSize(),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.Start
                ) {
                    Button(
                        modifier = Modifier
                            .size(60.dp)
                            .clickable {
                                navController.popBackStack()
                            },
                        onClick = { navController.popBackStack() },
                        colors = ButtonDefaults.buttonColors(
                            backgroundColor = Color.White,
                            contentColor = Color.White,
                            disabledContentColor = Color.White,
                            disabledBackgroundColor = Color.White,
                        ),
                        elevation = null
                    ) {
                        Image(
                            painter = painterResource(id = R.drawable.icon_back),
                            contentDescription = "Button with image",
                            contentScale = ContentScale.Fit
                        )
                    }

                    Text(
                        text = text,
                        color = Color.Black,
                        style = TextStyle(
                            fontFamily = rostelecomFontFamily,
                            fontWeight = FontWeight.Medium,
                            fontSize = 23.sp
                        )
                    )
                }
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.End
                ) {
                    if (QRBool) {
                        IconButton(onClick = {}
                        ) {
                            Icon(
                                painter = painterResource(
                                    id = R.drawable.icon_qr_code
                                ),
                                tint = Color.Black,
                                contentDescription = "Qr"
                            )
                        }
                    }

                    if (dotsBool) {
                        IconButton(
                            onClick = { expandedDots = !expandedDots },
                        ) {
                            Icon(
                                painter = painterResource(
                                    id = R.drawable.icon_more_vert
                                ),
                                tint = Color.Black,
                                contentDescription = "Иконка больше",
                            )
                        }
                    }
                    DropdownMenu(
                        expanded = expandedDots,
                        onDismissRequest = { expandedDots = false })
                    {
                        DropdownMenuItem(
                            onClick = {

                                expandedDots = false
                            }) {
                            Text("Редактировать")
                        }
                        DropdownMenuItem(
                            onClick = {
                                onClick()
                                expandedDots = false
                            }) {
                            Text("Удалить")
                        }
                    }

                }
            }
        }
    )
}
