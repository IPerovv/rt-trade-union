package com.example.rt.presentation.page_elements.cards

import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.rt.R
import com.example.rt.navigation.NavRoute
import com.example.rt.presentation.theme.BlueLinkColor
import com.example.rt.presentation.theme.rostelecomFontFamily

@Composable
fun RTCoinProfileCard(
    balance: Long,
    navController: NavController? = null
) {
    Card(
        //Карточка на которой расположен пост
        shape = RoundedCornerShape(14.dp),
        elevation = 5.dp,
        modifier = Modifier
            .fillMaxWidth()
            .padding(5.dp)
            .clip(RoundedCornerShape(14.dp))
            .clickable {
                navController?.currentBackStackEntry
                    ?.arguments
                    ?.putLong("BALANCE", balance)
                navController?.navigate(NavRoute.RTCoinScreen.route)
            },
    ) {
        Column(Modifier.padding(vertical = 7.dp)) {
            Row(
                Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically
            ) {

                Text(
                    modifier = Modifier.padding(start = 15.dp),
                    text = "RT coins",
                    fontFamily = rostelecomFontFamily,
                    fontWeight = FontWeight.Medium,
                    fontSize = 18.sp,
                )

                Row(
                    Modifier.padding(end = 15.dp),
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    RtCoinsCount(balance)
                    Spacer(modifier = Modifier.size(10.dp))
                    Image(
                        painter = painterResource(id = R.drawable.rt_coin),
                        contentDescription = "Rt Coin image",
                        Modifier.size(25.dp)
                    )
                }
            }
            Spacer(modifier = Modifier.size(3.dp))
            HowToGetCoins()
        }

    }
}

@Composable
private fun HowToGetCoins() {
    val swappableStr = remember {
        mutableStateOf("Как получить?")
    }

    Text(
        text = swappableStr.value,
        fontFamily = rostelecomFontFamily,
        fontWeight = FontWeight.Medium,
        fontSize = 12.sp,
        color = BlueLinkColor,
        modifier = Modifier
            .padding(start = 15.dp)
            .animateContentSize()
            .clickable {
                swappableStr.value =
                    " * RT coins можно получить за сканирование qr кодов на мероприятиях, за участие в конкурсах, за достижения новых высот на работе."
            }
    )
}

@Composable
private fun RtCoinsCount(localUserCoins: Long) {
    Text(
        text = localUserCoins.toString(),
        fontFamily = rostelecomFontFamily,
        fontWeight = FontWeight.Medium,
        fontSize = 23.sp,
    )
}


        