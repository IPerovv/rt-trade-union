package com.example.rt.presentation.main_menu_screens

import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.example.rt.domain.model.Feedback
import com.example.rt.presentation.event.FeedBackEvent
import com.example.rt.presentation.event.ValidationEvent
import com.example.rt.presentation.page_elements.app_bars.FeedbackScreenTopAppBar
import com.example.rt.presentation.theme.GrayBackgroundColor
import com.example.rt.presentation.theme.customAppTypography
import com.example.rt.presentation.viewmodel.FeedBackViewModel
import kotlinx.coroutines.launch

@Composable
fun FeedbackScreen(
    navController: NavController,
    feedBackViewModel: FeedBackViewModel = hiltViewModel()
) {
    val state = feedBackViewModel.state
    val coroutineScope = rememberCoroutineScope()
    val context = LocalContext.current

    Scaffold(
        modifier = Modifier.fillMaxSize(),
        content = {
            it
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .background(color = GrayBackgroundColor),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center,
            ) {
                FeedbackScreenTopAppBar(
                    "Обратная связь",
                    navController = navController,
                    onItemClick = {
                        coroutineScope.launch {
                            feedBackViewModel.onEvent(FeedBackEvent.SubmitData)
                            feedBackViewModel.validationEventChannel.collect { event ->
                                when (event) {
                                    is ValidationEvent.Success -> {
                                        navController.popBackStack()
                                        Toast.makeText(
                                            context,
                                            "Вы успешно отправили письмо!",
                                            Toast.LENGTH_SHORT
                                        ).show()
                                    }
                                    is ValidationEvent.Failure -> {
                                        Toast.makeText(
                                            context,
                                            "Произошла ошибка!",
                                            Toast.LENGTH_SHORT
                                        ).show()
                                    }
                                }
                            }
                        }
                    }
                )

                Card( //Карточка на которой расположен пост
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(top = 8.dp)
                        .verticalScroll(rememberScrollState()),
                    shape = RoundedCornerShape(topStart = 11.dp, topEnd = 11.dp),
                    elevation = 5.dp
                ) {
                    Box {
                        Column {
                            Spacer(modifier = Modifier.size(12.dp))
                            RTMail(
                                Feedback(
                                    RTMail = "rostelecom@gmail.com",
                                    UserMail = "ilia@gmail.com"
                                )
                            )
                            Spacer(modifier = Modifier.size(15.dp))
                            Spacer(
                                modifier = Modifier
                                    .height(1.dp)
                                    .fillMaxWidth()
                                    .background(color = GrayBackgroundColor)
                            )
                            OutlinedTextField(
                                modifier = Modifier
                                    .fillMaxSize(),
                                value = state.text,
                                textStyle = customAppTypography.subtitle1,

                                onValueChange = { newText ->
                                    feedBackViewModel.onEvent(FeedBackEvent.TextChanges(newText))
                                },

                                placeholder = { if (state.text.isEmpty()) Text("Введите ваше сообщение") },

                                colors = TextFieldDefaults.textFieldColors(
                                    textColor = Color.Black,
                                    disabledTextColor = Color.Transparent,
                                    focusedIndicatorColor = Color.Transparent,
                                    unfocusedIndicatorColor = Color.Transparent,
                                    disabledIndicatorColor = Color.Transparent,
                                    backgroundColor = Color.White
                                ),

                                keyboardOptions = KeyboardOptions(
                                    keyboardType = KeyboardType.Text,
                                    imeAction = ImeAction.Done
                                )
                            )
                        }
                    }
                }
            }
        }
    )
}

@Composable
private fun AdditionalSection() {
    Text(
        //Заголовок
        text = "Дополнительно:",
        style = customAppTypography.subtitle1,
        modifier = Modifier.padding(horizontal = 10.dp),
    )
}

@Composable
private fun RTMail(mail: Feedback) {
    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.Center
    ) {
        Text(
            //Заголовок
            style = customAppTypography.caption,
            text = mail.RTMail,
        )
    }
}
