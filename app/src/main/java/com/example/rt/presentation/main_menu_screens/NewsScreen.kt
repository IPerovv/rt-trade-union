package com.example.rt.presentation.main_menu_screens

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.FloatingActionButton
import androidx.compose.material.Icon
import androidx.compose.material.Scaffold
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.example.rt.navigation.NavRoute
import com.example.rt.presentation.event.NewsEvent
import com.example.rt.presentation.page_elements.app_bars.BottomNavBar
import com.example.rt.presentation.page_elements.app_bars.MainScreenTopAppBar
import com.example.rt.presentation.page_elements.cards.NewsCard
import com.example.rt.presentation.theme.GrayBackgroundColor
import com.example.rt.presentation.theme.Orange
import com.example.rt.presentation.viewmodel.NewsViewModel
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState

@RequiresApi(Build.VERSION_CODES.O)
@ExperimentalMaterialApi
@Composable
fun ListedNewsScreen(
    navController: NavController,
    newsViewModel: NewsViewModel = hiltViewModel()
) {
    val state = newsViewModel.state.collectAsState()

    val swipeRefreshState = rememberSwipeRefreshState(
        isRefreshing = newsViewModel.getLoadingState()
    )

    Scaffold(
        backgroundColor = GrayBackgroundColor,

        topBar = {
            MainScreenTopAppBar("Новости")
        },

        bottomBar = {
            BottomNavBar(navController = navController)
        },

        floatingActionButton = {
            FloatingActionButton(
                onClick = { navController.navigate(NavRoute.CreateNewsScreen.route) },
                backgroundColor = Orange
            ) {
                Icon(Icons.Filled.Add, contentDescription = "AddNews")
            }
        },

        content = { innerPadding ->
            SwipeRefresh(
                state = swipeRefreshState,
                onRefresh = { newsViewModel.onEvent(NewsEvent.Refresh) }
            ) {
                LazyColumn(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(innerPadding)
                ) {
                    items(state.value.listNews.size) { index ->
                        val news = state.value.listNews[index]
                        NewsCard(
                            viewModel = newsViewModel,
                            item = news,
                            onClick = {
                                navController.currentBackStackEntry
                                    ?.arguments
                                    ?.putParcelable("NEWS", news)
                                navController.navigate(NavRoute.SingleNewsScreen.route)
                            }
                        )
                    }
                }
            }
        }
    )
}