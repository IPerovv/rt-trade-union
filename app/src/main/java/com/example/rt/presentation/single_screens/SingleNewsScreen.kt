package com.example.rt.presentation.single_screens

import android.os.Build
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Card
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import com.example.rt.domain.model.News
import com.example.rt.navigation.NavRoute
import com.example.rt.presentation.event.NewsEvent
import com.example.rt.presentation.event.ValidationEvent
import com.example.rt.presentation.page_elements.*
import com.example.rt.presentation.page_elements.app_bars.SingleScreenTopBar
import com.example.rt.presentation.page_elements.content_parts.ContentDescription
import com.example.rt.presentation.page_elements.content_parts.ContentPicture
import com.example.rt.presentation.page_elements.content_parts.ContentPostedDate
import com.example.rt.presentation.page_elements.content_parts.ContentTitle
import com.example.rt.presentation.theme.GrayBackgroundColor
import com.example.rt.presentation.viewmodel.NewsViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun SingleNewsScreen(
    item: News,
    navController: NavHostController
) {
    val viewModel: NewsViewModel = hiltViewModel()
    val coroutineScope = rememberCoroutineScope()
    val context = LocalContext.current

    Scaffold(
        modifier = Modifier.fillMaxSize(),
        content = {
            it
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .background(color = GrayBackgroundColor),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center,
            ) {
                SingleScreenTopBar(
                    onClick = {
                        coroutineScope.launch {
                            viewModel.onEvent(NewsEvent.DeleteNews(item))
                            viewModel.validationEventChannel.collect { event ->
                                when (event) {
                                    is ValidationEvent.Success -> {
                                        Toast.makeText(
                                            context,
                                            "Новость удалена!",
                                            Toast.LENGTH_LONG
                                        ).show()
                                        delay(500)
                                        navController.navigate(NavRoute.NewsScreen.route)
                                    }
                                    is ValidationEvent.Failure -> {
                                        Toast.makeText(
                                            context,
                                            "Произошла ошибка!",
                                            Toast.LENGTH_LONG
                                        ).show()
                                    }
                                }
                            }
                        }
                    },
                    text = "Новости",
                    navController = navController
                )

                Card(
                    //Карточка на которой расположен пост
                    shape = RoundedCornerShape(14.dp),
                    elevation = 5.dp,
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(top = 6.dp)
                        .verticalScroll(rememberScrollState()),
                ) {
                    Box {
                        Column {
                            Spacer(modifier = Modifier.size(7.dp))
                            ContentPostedDate(item.date)  //Дата написания
                            Spacer(modifier = Modifier.size(3.dp))
                            ContentTitle(item.title)            //Заголовок
                            Spacer(modifier = Modifier.size(10.dp))
                            ContentPicture(
                                viewModel = hiltViewModel(),
                                photoUrl = item.photo
                            )          //Фото
                            Spacer(modifier = Modifier.size(12.dp))
                            ContentDescription(item.description) //Текст поста
                            Spacer(modifier = Modifier.size(8.dp))
                        }
                    }
                }
            }
        }
    )
}

