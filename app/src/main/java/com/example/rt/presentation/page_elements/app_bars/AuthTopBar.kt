package com.example.rt.presentation.page_elements.app_bars

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.rt.R
import com.example.rt.presentation.theme.rostelecomFontFamily


@Composable
fun AuthTopBar(
    text: String,
) {
    TopAppBar(
        backgroundColor = Color.White,
        elevation = 5.dp,
        content = {
            Row(
                modifier = Modifier.fillMaxSize(),
                horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    text = text,
                    color = Color.Black,
                    style = TextStyle(
                        fontFamily = rostelecomFontFamily,
                        fontWeight = FontWeight.Medium,
                        fontSize = 23.sp
                    )
                )
            }
        }
    )
}