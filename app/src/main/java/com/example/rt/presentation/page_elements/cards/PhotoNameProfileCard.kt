package com.example.rt.presentation.page_elements.cards

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import coil.compose.rememberImagePainter
import coil.request.CachePolicy
import coil.request.ImageRequest
import coil.transform.CircleCropTransformation
import com.example.rt.R
import com.example.rt.domain.model.User
import com.example.rt.presentation.theme.ClearWhite
import com.example.rt.presentation.theme.GrayBackgroundColor
import com.example.rt.presentation.theme.customAppTypography
import com.example.rt.presentation.viewmodel.ProfileViewModel

@Composable
fun PhotoNameProfileCard(
    viewModel: ProfileViewModel,
    user: User,
) {
    Card(
        //Карточка на которой расположены фото и фи пользователя в профиле
        shape = RoundedCornerShape(14.dp),
        elevation = 5.dp,
        modifier = Modifier
            .fillMaxWidth()
            .padding(5.dp)
            .clip(RoundedCornerShape(14.dp)),
        backgroundColor = GrayBackgroundColor
    ) {
        Column {
            Spacer(modifier = Modifier.size(45.dp))
            Box(
                modifier = Modifier
                    .height(155.dp)
                    .fillMaxWidth()
                    .background(color = ClearWhite)
            )
        }
        Column(horizontalAlignment = Alignment.CenterHorizontally) {
            Column {
                Spacer(modifier = Modifier.size(7.dp))
                ProfilePhoto(viewModel, user.photo)
                Spacer(modifier = Modifier.size(7.dp))
                ProfileNameText(user.firstName, user.lastName)
            }
        }
    }
}

@Composable
private fun ProfileNameText(name: String, lastName: String) {
    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.Center
    ) {
        Text(
            text = "$name $lastName",
            style = customAppTypography.caption
        )
    }
}

@Composable
private fun ProfilePhoto(
    viewModel: ProfileViewModel,
    photoUrl: String?,
) {
    val painter = rememberImagePainter(
        request = ImageRequest.Builder(LocalContext.current)
            .data("http://31.129.108.71:8080/files/$photoUrl")
            .transformations(CircleCropTransformation())
            .memoryCachePolicy(CachePolicy.ENABLED)
            .diskCachePolicy(CachePolicy.ENABLED) // Enable disk cache
            .networkCachePolicy(CachePolicy.ENABLED) // Enable network cache
            .build(),
        imageLoader = viewModel.getImageLoader()
    )

    Row(
        Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.Center
    ) {
        Image(
            painter = if (photoUrl?.isEmpty() == true) {
                painterResource(id = R.drawable.add_circle_button)
            } else {
                painter
            },
            contentDescription = "user_profile_photo",
            contentScale = ContentScale.Crop,
            modifier = Modifier
                .size(150.dp)
                .clip(CircleShape)
                .border(
                    BorderStroke(7.dp, GrayBackgroundColor),
                    CircleShape
                )
                .padding(7.dp)
        )
    }
}