package com.example.rt.presentation.page_elements.cards

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.unit.dp
import com.example.rt.domain.model.Document
import com.example.rt.presentation.theme.customAppTypography

@Composable
fun DocumentCard(
    item: Document,
    onClick: () -> Unit,
) {
    Card(
        //Карточка на которой расположен пост
        modifier = Modifier
            .fillMaxWidth()
            .padding(4.dp)
            .clip(RoundedCornerShape(14.dp))
            .clickable(onClick = onClick),
        shape = RoundedCornerShape(14.dp),
        elevation = 5.dp,
    ) {
        Box {
            Text(
                modifier = Modifier.padding(
                    top = 9.dp,
                    bottom = 8.dp,
                    start = 15.dp,
                    end = 15.dp
                ),
                maxLines = 2,
                text = item.title,
                style = customAppTypography.caption
            )
        }
    }
}