package com.example.rt.presentation.page_elements.logic_parts

fun reduceNumerals(numeral: Long): String {
    val returnStringBuilder = StringBuilder()

    return when {
        numeral < 1000 -> returnStringBuilder
            .append(numeral)
            .toString()
        numeral < 1000000 -> returnStringBuilder
            .append(numeral / 1000)
            .append(".")
            .append(numeral % 1000 / 100)
            .append("k")
            .toString()
        numeral < 1000000000 -> returnStringBuilder
            .append(numeral / 1000000)
            .append(".")
            .append(numeral % 1000 / 100)
            .append("m")
            .toString()
        else -> returnStringBuilder
            .append(numeral / 1000000000)
            .append("b")
            .toString()
    }
}