package com.example.rt.presentation.main_menu_screens

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.example.rt.domain.model.User
import com.example.rt.presentation.page_elements.app_bars.BottomNavBar
import com.example.rt.presentation.page_elements.app_bars.MainScreenTopAppBar
import com.example.rt.presentation.page_elements.cards.*
import com.example.rt.presentation.theme.GrayBackgroundColor
import com.example.rt.presentation.viewmodel.ProfileViewModel

@Composable
fun ProfileScreen(navController: NavController) {
    val viewModel: ProfileViewModel = hiltViewModel()
    val account = viewModel.getAccountManager()

    Scaffold(
        backgroundColor = GrayBackgroundColor,
        topBar = {
            MainScreenTopAppBar("Профиль", searchBool = false, QRBool = true)
        },
        bottomBar = {
            BottomNavBar(navController = navController)
        },
        content = { innerPadding ->
            Column(
                Modifier
                    .padding(innerPadding)
                    .verticalScroll(rememberScrollState())
            ) {
                PhotoNameProfileCard(
                    viewModel = viewModel,
                    user = User(
                        account.getPhotoUrl(),
                        account.getFirstName(),
                        account.getLastName(),
                        account.getRating()
                    ),
                )
                JoinProfsProfileCard(navController)
                RatingProfileCard(
                    viewModel = viewModel,
                    homeUser = User(
                        account.getPhotoUrl(),
                        account.getFirstName(),
                        account.getLastName(),
                        account.getRating()
                    ),
                    topUsers = viewModel.state.threeTopUser,
                    navController = navController,
                )
                RTCoinProfileCard(
                    balance = account.getBalance(),
                    navController = navController
                )
                GroupEventsProfileCard(
                    viewModel,
                    navController,
                )
            }
        }
    )
}