package com.example.rt.presentation.page_elements.content_parts

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import coil.compose.ImagePainter.State.Empty.painter
import coil.compose.rememberImagePainter
import coil.request.CachePolicy
import coil.request.ImageRequest
import com.example.rt.R
import com.example.rt.presentation.viewmodel.NewsViewModel

@Composable
fun ContentPicture(
    viewModel: NewsViewModel,
    photoUrl: String?
) {
    if (photoUrl?.isNotEmpty() == true && photoUrl != "null") {
        val painter = rememberImagePainter(
            request = ImageRequest.Builder(LocalContext.current)
                .data("http://31.129.108.71:8080/files/$photoUrl")
                .memoryCachePolicy(CachePolicy.ENABLED)
                .diskCachePolicy(CachePolicy.ENABLED)
                .networkCachePolicy(CachePolicy.ENABLED)
                .build(),
            imageLoader = viewModel.getImageLoader()
        )

        Image(
            modifier = Modifier
                .fillMaxWidth()
                .size(280.dp)
                .padding(horizontal = 10.dp)
                .clip(shape = RoundedCornerShape(14.dp)),
            contentScale = ContentScale.FillWidth,
            painter = painter,
            contentDescription = "image",
        )
    }
}


