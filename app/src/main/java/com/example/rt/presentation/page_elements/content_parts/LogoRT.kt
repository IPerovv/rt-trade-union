package com.example.rt.presentation.page_elements

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import com.example.rt.R
import com.example.rt.presentation.theme.rostelecomFontFamily

@Composable
fun LogoRt() {
    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.Bottom
    ) {
        Image(
            painter = painterResource(id = R.drawable.icon_logo_rt),
            contentDescription = null,
            contentScale = ContentScale.Crop
        )

        Text(
            text = "СОЮЗ",
            style = TextStyle(
                fontFamily = rostelecomFontFamily,
                fontWeight = FontWeight.Bold,
                fontSize = 30.sp
            )
        )
    }
}
