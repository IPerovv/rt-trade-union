package com.example.rt.presentation.page_elements.buttons

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.example.rt.R
import com.example.rt.presentation.theme.DarkBlueImInColor
import com.example.rt.presentation.theme.DarkGrayTypeColor
import com.example.rt.presentation.theme.GrayBasicColor
import com.example.rt.presentation.theme.buttonPostsType

@Composable
fun IParticipateButton(iParticipateBool: MutableState<Boolean>) {    //Кнопка "Я пойду"
    Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.End,
        modifier = Modifier
            .background(
                color = iParticipateBackgroundColorSwitch(iParticipateBool),
                shape = RoundedCornerShape(12.dp)
            )
            .clip(RoundedCornerShape(12.dp))
            .clickable { iParticipateBool.value = !iParticipateBool.value }
            .padding(
                horizontal = 5.dp,
                vertical = 5.dp
            )
    ) {
        Text(
            text = "Я пойду",
            color = likeCountColorSwitch(iParticipateBool),
            style = MaterialTheme.typography.buttonPostsType,
            modifier = Modifier.padding(start = 5.dp)
        )
        Image(
            painter = painterResource(id = iParticipateIconColorSwitch(iParticipateBool)),
            contentDescription = "ImInIcon",
            modifier = Modifier
                .padding(horizontal = 5.dp)
                .size(26.dp)
        )
    }
}


private fun iParticipateIconColorSwitch(iParticipateBool: MutableState<Boolean>): Int {
    return if (!iParticipateBool.value) {
        R.drawable.icon_i_participate_not_cliked
    } else {
        R.drawable.icon_i_participate_cliked
    }
}

private fun iParticipateBackgroundColorSwitch(iParticipateBool: MutableState<Boolean>): Color {
    return if (!iParticipateBool.value) GrayBasicColor else DarkBlueImInColor
}

private fun likeCountColorSwitch(iParticipateBool: MutableState<Boolean>): Color {
    return if (!iParticipateBool.value) DarkGrayTypeColor else Color.White
}