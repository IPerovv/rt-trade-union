package com.example.rt.presentation.theme

import androidx.compose.ui.graphics.Color

val lightGray = Color(0xFF575D68)
val Orange = Color(0xFFFF4F12)
val RedBackgroundThumbUpColor = Color(0xFFF84A4A)
val GrayBasicColor = Color(0xFFF3F3F4)
val GrayBackgroundColor = Color(0xFFE7E7E9)
val GrayInputColor = Color(0xFFF0F0F1)
val DarkGrayTypeColor = Color(0xFF575D68)
val DarkBlueImInColor = Color(0xFF101828)
val BlueLinkColor = Color(0xFF1F4496)
val ClearWhite = Color(0xFFFFFFFF)
val PinkLight = Color(0xFFD4AFFF)

