package com.example.rt.presentation.auth_screens

import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import com.example.rt.R
import com.example.rt.navigation.NavRoute
import com.example.rt.presentation.event.AuthFormEvent
import com.example.rt.presentation.event.ScenarioAuthEvent
import com.example.rt.presentation.event.ValidationEvent
import com.example.rt.presentation.page_elements.LogoRt
import com.example.rt.presentation.page_elements.app_bars.AuthTopBar
import com.example.rt.presentation.page_elements.buttons.ClassicButton
import com.example.rt.presentation.viewmodel.AuthViewModel
import kotlinx.coroutines.launch

@Composable
fun InputCodeScreen(
    navController: NavHostController,
    scenario: String?,
    viewModel: AuthViewModel = hiltViewModel()
) {
    val coroutineScope = rememberCoroutineScope()
    val context = LocalContext.current

    Scaffold(
        modifier = Modifier.fillMaxSize(),
        topBar = {
            AuthTopBar(
                if (scenario == ScenarioAuthEvent.RESTORE_ACCOUNT_PASSWORD.name) {
                    "Ввостановление пароля"
                } else {
                    "Создание аккаунта"
                },
            )
        },
        content = {
            it
            Image(
                modifier = Modifier.fillMaxSize(),
                painter = painterResource(id = R.drawable.background_auth),
                contentDescription = "Фон",
                contentScale = ContentScale.FillBounds
            )

            Column(
                modifier = Modifier.fillMaxSize(),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ) {
                Card(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(
                            top = 60.dp,
                            bottom = 60.dp,
                            start = 15.dp,
                            end = 15.dp
                        ),
                    shape = RoundedCornerShape(14.dp),
                    backgroundColor = Color.White,
                    elevation = 5.dp
                ) {
                    Column(
                        modifier = Modifier
                            .fillMaxSize()
                            .padding(
                                start = 25.dp,
                                end = 25.dp,
                                top = 20.dp,
                                bottom = 40.dp
                            ),
                    ) {
                        LogoRt()
                        Spacer(modifier = Modifier.height(90.dp))
                        Column(
                            modifier = Modifier.fillMaxSize(),
                            verticalArrangement = Arrangement.SpaceBetween
                        ) {
                            InfoScreens()

                            ClassicButton(
                                "Продолжить",
                                Modifier
                                    .fillMaxWidth()
                                    .height(45.dp),
                                onClick = {
                                    coroutineScope.launch {
                                        viewModel.onEvent(AuthFormEvent.ConfirmCodeSubmit)
                                        viewModel.validationEventChannel.collect { event ->
                                            when (event) {
                                                is ValidationEvent.Success -> {
                                                    navController.navigate(
                                                        if (scenario == ScenarioAuthEvent.REGISTER_ACCOUNT.name) {
                                                            NavRoute.InputPersonalDataScreen.route
                                                        } else {
                                                            NavRoute.AuthScreen.route
                                                        }
                                                    )
                                                }
                                                is ValidationEvent.Failure -> {
                                                    Toast.makeText(
                                                        context,
                                                        "Попробуйте еще раз!",
                                                        Toast.LENGTH_SHORT
                                                    ).show()
                                                }
                                            }
                                        }
                                    }
                                }
                            )
                        }
                    }
                }
            }
        }
    )
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun InfoScreens(
    viewModel: AuthViewModel = hiltViewModel()
) {
    val focusRequesters = remember { List(6) { FocusRequester() } }
    val textState = remember { List(6) { mutableStateOf("") } }
    val keyboardController = LocalSoftwareKeyboardController.current

    Column {
        Text(
            text = "Введите код,  отправленный вам на почту",
            style = TextStyle(
                color = Color.Black,
                fontSize = 18.sp,
                fontWeight = FontWeight.Bold
            ),
        )

        Spacer(modifier = Modifier.height(6.dp))

        Text(
            text = "Отправить еще раз",
            style = TextStyle(
                fontSize = 16.sp,
                fontWeight = FontWeight.Light
            ),
        )

        Spacer(modifier = Modifier.height(25.dp))

        Row {
            repeat(6) { index ->
                OutlinedTextField(
                    value = textState[index].value,

                    onValueChange = { newValue ->
                        if (newValue.length <= 1) {
                            textState[index].value = newValue.uppercase()

                            if (newValue.isNotEmpty()) {
                                viewModel.onEvent(
                                    AuthFormEvent.ConfirmCodeOnChanged(
                                        index,
                                        newValue.first().uppercaseChar()
                                    )
                                )
                            }

                            if (textState[index].value.isNotEmpty() && index < 5) {
                                focusRequesters[index + 1].requestFocus()
                            }
                        }

                        if (index == 5 && newValue.isNotEmpty()) {
                            keyboardController?.hide()
                        }
                    },

                    modifier = Modifier
                        .weight(1f)
                        .size(
                            width = TextFieldDefaults.MinWidth + 4.dp,
                            height = 65.dp
                        )
                        .focusRequester(focusRequesters[index]),

                    keyboardOptions = KeyboardOptions(
                        imeAction = if (index == 5) ImeAction.Done else ImeAction.Next
                    ),

                    singleLine = true,

                    maxLines = 1,

                    colors = TextFieldDefaults.textFieldColors(
                        textColor = Color.Black,
                        disabledTextColor = Color.Transparent,
                        focusedIndicatorColor = Color.Transparent,
                        unfocusedIndicatorColor = Color.Transparent,
                        disabledIndicatorColor = Color.Transparent
                    ),

                    textStyle = LocalTextStyle.current.copy(
                        fontSize = 16.sp,
                        textAlign = TextAlign.Center
                    ),

                    keyboardActions = KeyboardActions(
                        onNext = {
                            if (index < 5) {
                                focusRequesters[index + 1].requestFocus()
                            }
                        },
                    )
                )
                if (index != 5) {
                    Spacer(
                        modifier = Modifier
                            .height(65.dp)
                            .width(6.dp)
                    )
                }
            }
        }
    }
}