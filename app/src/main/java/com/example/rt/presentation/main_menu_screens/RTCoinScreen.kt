package com.example.rt.presentation.main_menu_screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.example.rt.R
import com.example.rt.domain.model.Article
import com.example.rt.presentation.page_elements.app_bars.SingleScreenTopBar
import com.example.rt.presentation.page_elements.cards.RTCoinCard
import com.example.rt.presentation.theme.GrayBackgroundColor
import com.example.rt.presentation.theme.GrayBasicColor

@Composable
fun RTCoinScreen(
    navController: NavHostController,
    balance: Long
) {
    Scaffold(backgroundColor = GrayBasicColor, topBar = {
        SingleScreenTopBar(
            onClick = {},
            text = "RT Coin",
            navController = navController,
            dotsBool = false
        )
    },
        modifier = Modifier
            .fillMaxSize(),
        content = {
            it
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .background(color = GrayBackgroundColor),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center,
            ) {
                Column() {
                    LazyColumn(
                        modifier = Modifier
                            .padding(top = 5.dp)
                            .fillMaxWidth(),
                    )
                    {
                        itemsIndexed(
                            listOf(
                                Article(
                                    coinsCost = 100,
                                    articleName = "Ручка",
                                    photo = R.drawable.product_pen
                                ),
                                Article(
                                    coinsCost = 200,
                                    articleName = "Блокнот",
                                    photo = R.drawable.product_textbook
                                ),
                                Article(
                                    coinsCost = 250,
                                    articleName = "Бейсболка",
                                    photo = R.drawable.product_baseball_cap
                                ),
                            )
                        ) { _, item ->
                            RTCoinCard(
                                balance = balance,
                                coinsCost = item.coinsCost,
                                articleName = item.articleName,
                                picture = item.photo,
                            )
                        }
                    }
                }
            }
        }
    )
}