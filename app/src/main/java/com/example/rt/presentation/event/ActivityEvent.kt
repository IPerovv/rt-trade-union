package com.example.rt.presentation.event

import com.example.rt.domain.model.GroupEvent

sealed class ActivityEvent {
    data class DeleteGroupEvent(val event: GroupEvent) : ActivityEvent()
    object Refresh : ActivityEvent()
}
