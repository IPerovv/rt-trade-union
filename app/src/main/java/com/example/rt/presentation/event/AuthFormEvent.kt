package com.example.rt.presentation.event

sealed class AuthFormEvent {
    data class EmailOnChanged(val email: String) : AuthFormEvent()
    data class PasswordOnChanged(val password: String) : AuthFormEvent()
    data class RepeatedPasswordOnChanged(val repeatedPassword: String) : AuthFormEvent()
    data class ConfirmCodeOnChanged(val index: Int, val letter: Char) : AuthFormEvent()
    object ConfirmCodeSubmit : AuthFormEvent()
    object RegistrationSubmit : AuthFormEvent()
    object AuthorizationSubmit : AuthFormEvent()
}
