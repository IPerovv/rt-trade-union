package com.example.rt.presentation.event

import okhttp3.MultipartBody

sealed class CreateNewsEvent {
    data class TitleChanges(val title: String) : CreateNewsEvent()
    data class DescriptionChanges(val description: String) : CreateNewsEvent()
    data class PhotoChanges(val photoPart: MultipartBody.Part?) : CreateNewsEvent()
    object SubmitNews : CreateNewsEvent()
}
