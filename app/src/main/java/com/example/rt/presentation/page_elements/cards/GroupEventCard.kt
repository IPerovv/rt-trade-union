package com.example.rt.presentation.page_elements.cards

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.rt.domain.model.GroupEvent
import com.example.rt.presentation.page_elements.buttons.CommentButton
import com.example.rt.presentation.page_elements.buttons.IParticipateButton
import com.example.rt.presentation.page_elements.buttons.LikeButton
import com.example.rt.presentation.page_elements.content_parts.ContentPicture
import com.example.rt.presentation.page_elements.content_parts.ContentTitle
import com.example.rt.presentation.page_elements.content_parts.DatetimePlaceBox
import com.example.rt.presentation.theme.customAppTypography


@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun GroupEventsCard(
    item: GroupEvent,
    onClick: () -> Unit,
) {
    val likeBool = remember {
        mutableStateOf(item.isLiked)
    }

    val imInBool = remember {
        mutableStateOf(item.isVisit)
    }

    Card( //Карточка на которой расположен пост с заголовком
        modifier = Modifier
            .fillMaxWidth()
            .padding(5.dp)
            .clip(RoundedCornerShape(14.dp))
            .clickable(onClick = onClick),
        shape = RoundedCornerShape(14.dp),
        elevation = 5.dp
    ) {
        Box {
            Column {
                Spacer(modifier = Modifier.size(7.dp))
                //ContentPostedDate(item.postDate) //Дата написания
                Spacer(modifier = Modifier.size(3.dp))
                ContentPicture(
                    viewModel = hiltViewModel(),
                    photoUrl = item.photo
                )  //Фото
                Spacer(modifier = Modifier.size(8.dp))
                ContentTitle(item.title)  //Заголовок
                Spacer(modifier = Modifier.size(3.dp))
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    DatetimePlaceBox( //Планируемые дата и время
                        item.date,
                        item.place,
                        customAppTypography.body1
                    )
                }
                Spacer(modifier = Modifier.size(20.dp))
                ButtonRow(
                    likeBool,
                    imInBool,
                    item
                ) //Строка с кнопками
                Spacer(modifier = Modifier.size(10.dp))
            }
        }
    }
}

@Composable
private fun ButtonRow(
    likeBool: MutableState<Boolean>,
    imInBool: MutableState<Boolean>,
    item: GroupEvent
) {
    Row(
        //Нижний ряд под кнопки ThumbUp, Comment, "Я пойду"
        modifier = Modifier
            .padding(horizontal = 10.dp)
            .fillMaxWidth(),
        horizontalArrangement = Arrangement.SpaceBetween,
    ) {
        Row { //Row объединяющий кнопку лайка и коммента
            LikeButton(
                viewModel = hiltViewModel(),
                newsId = 2,
                likeBool = likeBool.value,
                likesCount = item.cntLikes
            )
            Spacer(Modifier.width(10.dp))
            CommentButton(item.cntComments)
        }
        IParticipateButton(iParticipateBool = imInBool) //Кнопка "Я пойду"
    }
}



