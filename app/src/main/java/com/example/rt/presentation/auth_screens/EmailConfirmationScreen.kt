package com.example.rt.presentation.auth_screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.example.rt.R
import com.example.rt.navigation.NavRoute
import com.example.rt.presentation.event.ScenarioAuthEvent
import com.example.rt.presentation.page_elements.LogoRt
import com.example.rt.presentation.page_elements.app_bars.AuthTopBar
import com.example.rt.presentation.page_elements.buttons.ClassicButton

@Composable
fun EmailConfirmationScreen(navController: NavHostController, scenario: String?) {
    Scaffold(
        modifier = Modifier.fillMaxSize(),
        content = {it
            Image(
                modifier = Modifier.fillMaxSize(),
                painter = painterResource(id = R.drawable.background_auth),
                contentDescription = "Фон",
                contentScale = ContentScale.FillBounds
            )

            Column(
                modifier = Modifier.fillMaxSize(),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ) {
                AuthTopBar(
                    if (scenario == ScenarioAuthEvent.RESTORE_ACCOUNT_PASSWORD.name) {
                        "Ввостановление пароля"
                    } else {
                        "Создание аккаунта"
                    },
                )

                Card(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(
                            top = 60.dp,
                            bottom = 60.dp,
                            start = 15.dp,
                            end = 15.dp
                        ),
                    shape = RoundedCornerShape(14.dp),
                    backgroundColor = Color.White,
                    elevation = 5.dp
                ) {
                    Column(
                        modifier = Modifier
                            .fillMaxSize()
                            .padding(
                                start = 30.dp,
                                end = 30.dp,
                                top = 15.dp,
                                bottom = 40.dp
                            ),
                    ) {
                        LogoRt()
                        Spacer(modifier = Modifier.height(85.dp))

                        Column(
                            modifier = Modifier.fillMaxSize(),
                            verticalArrangement = Arrangement.SpaceBetween
                        ) {
                            InfoScreen(scenario)
                            ClassicButton(
                                "Продолжить",
                                Modifier
                                    .fillMaxWidth()
                                    .height(45.dp),
                                onClick = {
                                    navController.navigate(
                                        "${NavRoute.InputCodeScreen.route}/${scenario}"
                                    )
                                }
                            )
                        }
                    }
                }
            }
        }
    )
}

@Composable
fun InfoScreen(scenario: String?) {
    val email = remember {
        mutableStateOf("")
    }

    val scenarioText = if (scenario == ScenarioAuthEvent.RESTORE_ACCOUNT_PASSWORD.name) {
        "восстановления пароля"
    } else {
        "создания аккаунта"
    }

    Column {
        Text(
            text = "Укажите почту, на которую мы сможем отправить вам письмо\nдля $scenarioText.",
            style = TextStyle(color = Color.LightGray),
        )

        OutlinedTextField(
            value = email.value,

            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 14.dp)
                .height(IntrinsicSize.Min),

            placeholder = { if (email.value.isEmpty()) Text("Почта") },

            onValueChange = { currentInputEmail ->
                email.value = currentInputEmail
            },

            colors = TextFieldDefaults.textFieldColors(
                textColor = Color.Gray,
                disabledTextColor = Color.Transparent,
                focusedIndicatorColor = Color.Transparent,
                unfocusedIndicatorColor = Color.Transparent,
                disabledIndicatorColor = Color.Transparent
            ),
        )
    }
}