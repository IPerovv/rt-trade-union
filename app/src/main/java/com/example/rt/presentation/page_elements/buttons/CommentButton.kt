package com.example.rt.presentation.page_elements.buttons

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.example.rt.R
import com.example.rt.presentation.page_elements.logic_parts.reduceNumerals
import com.example.rt.presentation.theme.DarkGrayTypeColor
import com.example.rt.presentation.theme.GrayBasicColor
import com.example.rt.presentation.theme.buttonPostsType

@Composable
fun CommentButton(commentCount: Long){ //Кнопка "Коммментарии"
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .background(
                color = GrayBasicColor,
                shape = RoundedCornerShape(12.dp)
            )
            .clip(RoundedCornerShape(12.dp))
            .clickable {/*TODO()*/}
            .padding(
                horizontal = 5.dp,
                vertical = 5.dp
            )
    ) {
        Image(
            painter = painterResource(id = R.drawable.icon_comment),
            contentDescription = "comment icon",
            modifier = Modifier
                .padding(horizontal = 5.dp)
                .size(25.dp)
        )
        Text(
            text = reduceNumerals(commentCount),
            color = DarkGrayTypeColor,
            style = MaterialTheme.typography.buttonPostsType,
            modifier = Modifier.padding(end = 3.dp)
        )
    }
}