package com.example.rt.presentation.auth_screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.example.rt.R
import com.example.rt.presentation.page_elements.LogoRt
import com.example.rt.presentation.page_elements.app_bars.AuthTopBar
import com.example.rt.presentation.page_elements.buttons.ClassicButton

@Composable
fun RestoreAccountScreen(navController: NavHostController) {
    Scaffold(
        modifier = Modifier.fillMaxSize(),
        content = { it
            Image(
                modifier = Modifier.fillMaxSize(),
                painter = painterResource(id = R.drawable.background_auth),
                contentDescription = "Фон",
                contentScale = ContentScale.FillBounds
            )

            Column(
                modifier = Modifier.fillMaxSize(),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ) {
                AuthTopBar(
                    "Ввостановление пароля",
                )

                Card(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(vertical = 40.dp, horizontal = 15.dp),
                    shape = RoundedCornerShape(14.dp),
                    backgroundColor = Color.White,
                    elevation = 5.dp
                ) {
                    Column(
                        modifier = Modifier
                            .fillMaxSize()
                            .padding(vertical = 30.dp, horizontal = 15.dp),
                    ) {
                        LogoRt()
                        Spacer(modifier = Modifier.height(75.dp))

                        Column(
                            modifier = Modifier.fillMaxSize(),
                            verticalArrangement = Arrangement.SpaceBetween
                        ) {
                            InputFieldsRestoreAccount()
                            ClassicButton(
                                "Продолжить",
                                Modifier
                                    .fillMaxWidth()
                                    .height(45.dp),
                                onClick = {

                                }
                            )
                        }
                    }
                }
            }
        }
    )
}


@Composable
fun InputFieldsRestoreAccount() {
    val inputPassword = remember {
        mutableStateOf("")
    }

    val inputRepeatedPassword = remember {
        mutableStateOf("")
    }

    Column(
        modifier = Modifier.fillMaxWidth(),
        verticalArrangement = Arrangement.SpaceBetween
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
        ) {
            OutlinedTextField(
                value = inputPassword.value,

                modifier = Modifier
                    .fillMaxWidth()
                    .height(IntrinsicSize.Min),

                placeholder = { if (inputPassword.value.isEmpty()) Text("Пароль") },

                onValueChange = { currentInputPassword ->
                    inputPassword.value = currentInputPassword
                },

                colors = TextFieldDefaults.textFieldColors(
                    textColor = Color.Gray,
                    disabledTextColor = Color.Transparent,
                    focusedIndicatorColor = Color.Transparent,
                    unfocusedIndicatorColor = Color.Transparent,
                    disabledIndicatorColor = Color.Transparent
                ),
            )

            InformationValidationPassword()

            OutlinedTextField(
                value = inputRepeatedPassword.value,

                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 20.dp)
                    .height(IntrinsicSize.Min),

                placeholder = { if (inputRepeatedPassword.value.isEmpty()) Text("Повторить пароль") },

                onValueChange = { currentInputPassword ->
                    inputRepeatedPassword.value = currentInputPassword
                },

                colors = TextFieldDefaults.textFieldColors(
                    textColor = Color.Gray,
                    disabledTextColor = Color.Transparent,
                    focusedIndicatorColor = Color.Transparent,
                    unfocusedIndicatorColor = Color.Transparent,
                    disabledIndicatorColor = Color.Transparent
                ),
            )
        }
    }
}


@Composable
fun InformationValidationPassword() {
    val spaceModifierPaddingLetters = Modifier.padding(top = 5.dp)

    Column(
        modifier = Modifier.padding(start = 10.dp, top = 15.dp),
    ) {
        Text(
            text = "Пароль должен содержать:",
            style = TextStyle(color = Color.LightGray),
        )

        Text(
            text = "Латинские заглавные буквы(AB)",
            style = TextStyle(color = Color.LightGray),
            modifier = spaceModifierPaddingLetters
        )

        Text(
            text = "Латинские строчные буквы(ab)",
            style = TextStyle(color = Color.LightGray),
            modifier = spaceModifierPaddingLetters
        )

        Text(
            text = "Знаки пунктуации(.*)",
            style = TextStyle(color = Color.LightGray),
            modifier = spaceModifierPaddingLetters
        )

        Text(
            text = "Более 6 символов",
            style = TextStyle(color = Color.LightGray),
            modifier = spaceModifierPaddingLetters
        )
    }
}
