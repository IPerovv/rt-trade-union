package com.example.rt.presentation.page_elements.app_bars

import androidx.compose.foundation.layout.*
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.rt.R
import com.example.rt.presentation.theme.rostelecomFontFamily

@Composable
fun MainScreenTopAppBar(
    title: String,
    searchBool: Boolean = true,
    QRBool: Boolean = false
) {
    TopAppBar(
        backgroundColor = Color.White,
        elevation = 5.dp,
        content = {
            Row(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(start = 18.dp),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Icon(
                    painter = painterResource(
                        id = R.drawable.logo_rt_mini
                    ),
                    contentDescription = "LogoRT",
                    tint = Color.Black
                )

                Spacer(Modifier.width(25.dp))
                Text(
                    text = title,
                    color = Color.Black,
                    style = TextStyle(
                        fontFamily = rostelecomFontFamily,
                        fontWeight = FontWeight.Medium,
                        fontSize = 23.sp
                    )
                )

                Spacer(
                    Modifier.weight(1f, true)
                )

                if (QRBool) {
                    IconButton(onClick = {
                    }) {
                        Icon(
                            painter = painterResource(
                                id = R.drawable.icon_qr_code
                            ),
                            tint = Color.Black,
                            contentDescription = "Qr"
                        )
                    }
                }

                if (searchBool) {
                    IconButton(onClick = {

                    }) {
                        Icon(
                            painter = painterResource(
                                id = R.drawable.icon_search
                            ),
                            tint = Color.Black,
                            contentDescription = "Поиск"
                        )
                    }
                }

                IconButton(onClick = {

                }) {
                    Icon(
                        painter = painterResource(
                            id = R.drawable.icon_notifications
                        ),
                        tint = Color.Black,
                        contentDescription = "Уведомления"
                    )
                }
            }
        }
    )
}