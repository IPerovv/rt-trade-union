package com.example.rt.presentation.page_elements.cards

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import coil.compose.rememberImagePainter
import coil.request.CachePolicy
import coil.request.ImageRequest
import com.example.rt.navigation.NavRoute
import com.example.rt.presentation.theme.customAppTypography
import com.example.rt.presentation.viewmodel.ProfileViewModel


@Composable
fun GroupEventsProfileCard(
    viewModel: ProfileViewModel,
    navController: NavController? = null
) {
    Card(
        //Карточка на которой расположена блок с фотографиями мероприятий
        shape = RoundedCornerShape(14.dp),
        elevation = 5.dp,
        modifier = Modifier
            .fillMaxWidth()
            .padding(5.dp)
            .clip(RoundedCornerShape(14.dp))
            .clickable {
                navController?.navigate(NavRoute.EventScreen.route)
            },
    ) {
        Column(Modifier.fillMaxWidth()) {
            Spacer(modifier = Modifier.size(7.dp))
            TextGroupEvents()
            Spacer(modifier = Modifier.size(7.dp))
            if (viewModel.state.fiveGroupEvents.isNotEmpty()) {
                EventPicturesRow(viewModel)
                Spacer(modifier = Modifier.size(13.dp))
            }
        }
    }
}

@Composable
private fun TextGroupEvents() {
    Row(
        Modifier
            .fillMaxWidth()
            .padding(horizontal = 15.dp),
        horizontalArrangement = Arrangement.SpaceBetween,
    ) {
        Text(
            text = "Календарь событий",
            style = customAppTypography.caption
        )
        Text(
            text = ">",
            style = customAppTypography.caption
        )
    }
}

@Composable
private fun EventPicturesRow(viewModel: ProfileViewModel) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 4.dp),
        horizontalArrangement = Arrangement.Center,
    ) {
        Spacer(modifier = Modifier.size(10.dp))
        val listEvents = viewModel.state.fiveGroupEvents
        listEvents.forEach { item ->
            if (item.photo.isNotEmpty()) {
                EventPicture(viewModel, item.photo)
                Spacer(modifier = Modifier.size(10.dp))
            }
        }
    }
}

@Composable
private fun EventPicture(
    viewModel: ProfileViewModel,
    photoUrl: String
) {
    val painter = rememberImagePainter(
        request = ImageRequest.Builder(LocalContext.current)
            .data("http://31.129.108.71:8080/files/$photoUrl")
            .memoryCachePolicy(CachePolicy.ENABLED)
            .diskCachePolicy(CachePolicy.ENABLED)
            .networkCachePolicy(CachePolicy.ENABLED)
            .build(),
        imageLoader = viewModel.getImageLoader()
    )

    Image(
        painter = painter,
        contentDescription = "Event Picture",
        contentScale = ContentScale.Crop,
        modifier = Modifier
            .size(60.dp)
            .aspectRatio(19f / 20f)
            .clip(RoundedCornerShape(11.dp))
    )
}



