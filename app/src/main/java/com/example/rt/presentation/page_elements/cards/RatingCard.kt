package com.example.rt.presentation.page_elements.cards

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.example.rt.domain.model.User
import com.example.rt.presentation.theme.RatingType
import com.example.rt.presentation.theme.customAppTypography


@Composable
fun RatingCard(
    user: User,
    ratingPlace: Int,
    modifier: Modifier
) {
    Card(
        //Карточка на которой расположен пост
        modifier = Modifier
            .fillMaxWidth()
            .padding(4.dp),
        shape = RoundedCornerShape(11.dp),
        elevation = 5.dp,
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = modifier
                .fillMaxWidth()
                .padding(7.dp)
        ) {
            Row(
                horizontalArrangement = Arrangement.Start,
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Text(
                    text = ratingPlace.toString(),
                    style = customAppTypography.subtitle1
                )
                Spacer(modifier = Modifier.size(15.dp))
                Image(
                    painter = painterResource(id = user.photo!!.toInt()),
                    contentDescription = "User photo",
                    contentScale = ContentScale.Crop,
                    modifier = Modifier
                        .size(40.dp)
                        .clip(CircleShape)
                )
                Spacer(modifier = Modifier.size(10.dp))
                Text(
                    text = user.firstName + " " + user.lastName,
                    style = MaterialTheme.typography.RatingType
                )
            }
            Text(
                text = user.rating.toString() + " " + "б",
                style = MaterialTheme.typography.RatingType
            )
        }
    }
}