package com.example.rt.presentation.page_elements.app_bars

import androidx.compose.material.BottomNavigation
import androidx.compose.material.BottomNavigationItem
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.compose.currentBackStackEntryAsState
import com.example.rt.navigation.nav_item.BottomNavItem
import com.example.rt.presentation.theme.Orange


@Composable
fun BottomNavBar(
    navController: NavController,
) {
    val listBottomItems = listOf(
        BottomNavItem.NewsScreen,
        BottomNavItem.EventScreen,
        BottomNavItem.DocumentScreen,
        BottomNavItem.ProfileScreen
    )

    BottomNavigation(
        backgroundColor = Color.White,
        elevation = 5.dp
    ) {
        val backStackEntry by navController.currentBackStackEntryAsState()
        val currentRout = backStackEntry?.destination?.route

        listBottomItems.forEach { item ->
            BottomNavigationItem(
                selected = currentRout == item.route,
                onClick = {
                    if (currentRout != item.route) {
                        navController.popBackStack()
                        navController.navigate(item.route)
                    }
                },
                icon = {
                    Icon(
                        painter = painterResource(id = item.iconId),
                        contentDescription = item.title
                    )
                },
                label = {
                    Text(text = item.title, fontSize = 12.sp)
                },
                selectedContentColor = Orange,
                unselectedContentColor = Color.Gray
            )
        }
    }
}