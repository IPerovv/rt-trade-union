package com.example.rt.presentation.event

data class FeedBackState(
    val text: String = ""
)
