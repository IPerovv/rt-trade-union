package com.example.rt.presentation.event

import okhttp3.MultipartBody

sealed class CreateGroupEventEvent {
    data class TitleChanges(val title: String) : CreateGroupEventEvent()
    data class DescriptionChanges(val description: String) : CreateGroupEventEvent()
    data class PhotoChanges(val photo: MultipartBody.Part?) : CreateGroupEventEvent()
    data class DateTimeChanges(val dateTime: String) : CreateGroupEventEvent()
    data class PlaceChanges(val place: String) : CreateGroupEventEvent()
    data class TimeChanges(val time: String) : CreateGroupEventEvent()
    object SubmitGroupEvent : CreateGroupEventEvent()
}
