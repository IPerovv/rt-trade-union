package com.example.rt.presentation.single_screens

import android.os.Build
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import com.example.rt.domain.model.GroupEvent
import com.example.rt.navigation.NavRoute
import com.example.rt.presentation.event.ActivityEvent
import com.example.rt.presentation.event.ValidationEvent
import com.example.rt.presentation.page_elements.*
import com.example.rt.presentation.page_elements.app_bars.SingleScreenTopBar
import com.example.rt.presentation.page_elements.content_parts.*
import com.example.rt.presentation.theme.GrayBackgroundColor
import com.example.rt.presentation.theme.GroupEventInfoType
import com.example.rt.presentation.viewmodel.GroupEventViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun SingleGroupEventScreen(
    item: GroupEvent,
    navController: NavHostController
) {
    val viewModel: GroupEventViewModel = hiltViewModel()
    val coroutineScope = rememberCoroutineScope()
    val context = LocalContext.current

    Scaffold(
        modifier = Modifier.fillMaxSize(),
        content = {
            it
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .background(color = GrayBackgroundColor),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center,
            ) {
                SingleScreenTopBar(
                    onClick = {
                        coroutineScope.launch {
                            viewModel.onEvent(ActivityEvent.DeleteGroupEvent(item))
                            viewModel.validationEventChannel.collect { event ->
                                when (event) {
                                    is ValidationEvent.Success -> {
                                        Toast.makeText(
                                            context,
                                            "Мероприятие удалено!",
                                            Toast.LENGTH_LONG
                                        ).show()
                                        delay(500)
                                        navController.navigate(NavRoute.EventScreen.route)
                                    }
                                    is ValidationEvent.Failure -> {
                                        Toast.makeText(
                                            context,
                                            "Произошла ошибка!",
                                            Toast.LENGTH_LONG
                                        ).show()
                                    }
                                }
                            }
                        }
                    },
                    "События",
                    navController = navController,
                    QRBool = true
                )

                Card(
                    //Карточка на которой расположен пост
                    shape = RoundedCornerShape(14.dp),
                    elevation = 5.dp,
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(top = 6.dp)
                        .verticalScroll(rememberScrollState()),
                ) {
                    Box {
                        Column {
                            Spacer(modifier = Modifier.size(7.dp))
                            ContentPostedDate(item.postDate)            //Дата написания
                            Spacer(modifier = Modifier.size(3.dp))
                            ContentTitle(item.title)                    //Заголовок
                            Spacer(modifier = Modifier.size(10.dp))
                            ContentPicture(
                                viewModel = hiltViewModel(),
                                photoUrl = item.photo
                            )                  //Фото
                            Spacer(modifier = Modifier.size(7.dp))
                            DatetimePlaceBox(                           //Время, дата, место
                                item.date,
                                item.place,
                                MaterialTheme.typography.GroupEventInfoType
                            )
                            Spacer(modifier = Modifier.size(8.dp))
                            ContentDescription(item.description)        //Текст поста
                            Spacer(modifier = Modifier.size(8.dp))
                        }
                    }
                }
            }
        }
    )
}
