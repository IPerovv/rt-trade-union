package com.example.rt.presentation.event

import com.example.rt.domain.model.News

sealed class NewsEvent {
    data class Like(val newsId: Int) : NewsEvent()
    data class UnLike(val newsId: Int) : NewsEvent()
    data class DeleteNews(val news: News) : NewsEvent()

    object Refresh : NewsEvent()
}
