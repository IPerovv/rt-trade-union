package com.example.rt.presentation.page_elements.content_parts

import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.example.rt.presentation.theme.customAppTypography

@Composable
fun ContentDescription(description: String) {
    Text(
        //Заголовок
        text = description,
        style = customAppTypography.body1,
        modifier = Modifier.padding(horizontal = 10.dp),
    )
}