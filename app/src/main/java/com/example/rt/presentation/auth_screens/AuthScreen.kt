package com.example.rt.presentation.auth_screens

import android.util.Log
import android.widget.Toast
import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import com.example.rt.R
import com.example.rt.navigation.NavRoute
import com.example.rt.presentation.event.AuthFormEvent
import com.example.rt.presentation.event.ScenarioAuthEvent
import com.example.rt.presentation.event.ValidationEvent
import com.example.rt.presentation.page_elements.LogoRt
import com.example.rt.presentation.page_elements.app_bars.AuthTopBar
import com.example.rt.presentation.page_elements.buttons.ClassicButton
import com.example.rt.presentation.theme.GrayInputColor
import com.example.rt.presentation.theme.Orange
import com.example.rt.presentation.viewmodel.AuthViewModel
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.pagerTabIndicatorOffset
import com.google.accompanist.pager.rememberPagerState
import kotlinx.coroutines.launch

@Composable
fun AuthScreen(
    navController: NavHostController,
) {
    Scaffold(
        modifier = Modifier.fillMaxSize(),
        content = {
            it
            Image(
                modifier = Modifier.fillMaxSize(),
                painter = painterResource(id = R.drawable.background_auth),
                contentDescription = "Фон",
                contentScale = ContentScale.FillBounds
            )

            Column(
                modifier = Modifier.fillMaxSize(),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ) {
                AuthTopBar("Авторизация")

                Card(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(vertical = 40.dp, horizontal = 15.dp),
                    shape = RoundedCornerShape(14.dp),
                    backgroundColor = Color.White,
                    elevation = 5.dp
                ) {
                    Column(
                        modifier = Modifier
                            .fillMaxSize()
                            .padding(
                                start = 15.dp,
                                end = 15.dp,
                                bottom = 30.dp,
                                top = 30.dp
                            ),
                        verticalArrangement = Arrangement.SpaceBetween
                    ) {
                        LogoRt()
                        Column(
                            modifier = Modifier
                                .fillMaxSize()
                                .padding(top = 50.dp),
                        ) {
                            SwitchMenuAuth(navController)
                        }
                    }
                }
            }
        },
    )
}

@OptIn(ExperimentalPagerApi::class)
@Composable
fun SwitchMenuAuth(
    navController: NavController,
    viewModel: AuthViewModel = hiltViewModel()
) {
    val tabSwitchMenuAuth = listOf("Регистрация", "Вход")
    val pagerState = rememberPagerState()
    val tabIndex = pagerState.currentPage
    val coroutineScope = rememberCoroutineScope()

    Column(
        modifier = Modifier.fillMaxSize()
    ) {
        TabRow(
            modifier = Modifier
                .clip(RoundedCornerShape(18.dp))
                .border(0.7.dp, GrayInputColor, RoundedCornerShape(18.dp))
                .animateContentSize(),
            selectedTabIndex = tabIndex,
            indicator = { tabPosition ->
                TabRowDefaults.Indicator(
                    modifier = Modifier.pagerTabIndicatorOffset(pagerState, tabPosition),
                    color = Color.Transparent,
                )
            },
            divider = {},
            backgroundColor = GrayInputColor,
            contentColor = Color.Transparent
        ) {
            tabSwitchMenuAuth.forEachIndexed { index, text ->
                val isSelected = index == tabIndex
                val backgroundColor by animateColorAsState(
                    if (isSelected) Orange else GrayInputColor
                )
                Tab(
                    modifier = Modifier
                        .clip(RoundedCornerShape(18.dp))
                        .background(backgroundColor),
                    selected = isSelected,
                    onClick = {
                        coroutineScope.launch {
                            pagerState.animateScrollToPage(index)
                        }
                    },
                    text = {
                        Text(text = text, color = Color.Black)
                    },
                )
            }
        }

        HorizontalPager(
            count = tabSwitchMenuAuth.size,
            state = pagerState,
            modifier = Modifier.fillMaxSize()
        ) { index ->
            when (index) {
                0 -> InputFieldsRegistration(navController, viewModel)
                1 -> InputFieldsAuth(navController, viewModel)
            }
        }
    }
}

@Composable
fun InputFieldsAuth(
    navController: NavController,
    viewModel: AuthViewModel = hiltViewModel()
) {
    val coroutineScope = rememberCoroutineScope()
    val passwordVisibility = remember { mutableStateOf(false) }
    val state = viewModel.state

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(top = 55.dp),
        verticalArrangement = Arrangement.SpaceBetween
    ) {
        OutlinedTextField(
            modifier = Modifier
                .fillMaxWidth()
                .height(IntrinsicSize.Min),
            value = state.email,

            textStyle = TextStyle(
                color = Color.Black,
                fontSize = 16.sp
            ),

            placeholder = { if (state.email.isEmpty()) Text("Почта") },

            colors = TextFieldDefaults.textFieldColors(
                textColor = Color.Gray,
                disabledTextColor = Color.Transparent,
                focusedIndicatorColor = Color.Transparent,
                unfocusedIndicatorColor = Color.Transparent,
                disabledIndicatorColor = Color.Transparent
            ),

            onValueChange = { currentInputLogin ->
                viewModel.onEvent(AuthFormEvent.EmailOnChanged(currentInputLogin))
            },

            singleLine = true,

            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Email,
                imeAction = ImeAction.Done
            )
        )

        OutlinedTextField(
            modifier = Modifier
                .fillMaxWidth(),
            value = state.password,

            textStyle = TextStyle(
                color = Color.Black,
                fontSize = 16.sp
            ),

            placeholder = { if (state.password.isEmpty()) Text("Пароль") },

            colors = TextFieldDefaults.textFieldColors(
                textColor = Color.Gray,
                disabledTextColor = Color.Transparent,
                focusedIndicatorColor = Color.Transparent,
                unfocusedIndicatorColor = Color.Transparent,
                disabledIndicatorColor = Color.Transparent
            ),

            onValueChange = { currentPassword ->
                viewModel.onEvent(AuthFormEvent.PasswordOnChanged(currentPassword))
            },

            trailingIcon = {
                IconButton(
                    onClick = { passwordVisibility.value = !passwordVisibility.value }
                ) {
                    Icon(
                        painter = painterResource(
                            id = if (passwordVisibility.value) {
                                R.drawable.icon_vicibility
                            } else {
                                R.drawable.icon_visibility_off
                            }
                        ),
                        contentDescription = null
                    )
                }
            },

            visualTransformation = if (passwordVisibility.value) {
                VisualTransformation.None
            } else {
                PasswordVisualTransformation()
            },

            singleLine = true,

            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Password,
                imeAction = ImeAction.Next
            )
        )

        TextButton(onClick = {
            navController.navigate(
                "${NavRoute.EmailConfirmationScreen.route}/${ScenarioAuthEvent.RESTORE_ACCOUNT_PASSWORD.name}"
            )
        }) {
            Text(
                text = "Восстановить пароль",
                style = TextStyle(
                    color = Color.Gray,
                    fontSize = 16.sp,
                ),
            )
        }

        val context = LocalContext.current
        ClassicButton(
            "Продолжить",
            Modifier
                .fillMaxWidth()
                .height(45.dp),
            onClick = {
                coroutineScope.launch {
                    viewModel.onEvent(AuthFormEvent.AuthorizationSubmit)
                    viewModel.authValidationEventChannel.collect { event ->
                        when (event) {
                            is ValidationEvent.Success -> {
                                Log.i("sus", "is good")
                                Toast.makeText(
                                    context,
                                    "Добро пожаловать!",
                                    Toast.LENGTH_SHORT
                                ).show()
                                navController.popBackStack()
                                navController.navigate(NavRoute.NewsScreen.route)
                            }
                            is ValidationEvent.Failure -> {
                                Toast.makeText(
                                    context,
                                    "Попробуйте еще раз!",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                    }
                }
            }
        )
    }
}

@Composable
fun InputFieldsRegistration(
    navController: NavController,
    viewModel: AuthViewModel = hiltViewModel()
) {
    val passwordVisibility = remember { mutableStateOf(false) }
    val repeatedPasswordVisibility = remember { mutableStateOf(false) }
    val state = viewModel.state
    val coroutineScope = rememberCoroutineScope()

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(top = 45.dp),
        verticalArrangement = Arrangement.SpaceBetween
    ) {
        OutlinedTextField(
            modifier = Modifier
                .fillMaxWidth()
                .height(IntrinsicSize.Min),

            value = state.email,

            isError = state.emailError != null,

            textStyle = TextStyle(
                color = Color.Black,
                fontSize = 16.sp
            ),

            placeholder = { if (state.email.isEmpty()) Text("Почта") },

            colors = TextFieldDefaults.textFieldColors(
                textColor = Color.Gray,
                disabledTextColor = Color.Transparent,
                focusedIndicatorColor = Color.Transparent,
                unfocusedIndicatorColor = Color.Transparent,
                disabledIndicatorColor = Color.Transparent
            ),

            onValueChange = { currentInputLogin ->
                viewModel.onEvent(AuthFormEvent.EmailOnChanged(currentInputLogin))
            },

            singleLine = true,

            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Password,
                imeAction = ImeAction.Done
            )
        )

        if (state.emailError != null) {
            Text(
                text = state.emailError,
                color = MaterialTheme.colors.error
            )
        }

        OutlinedTextField(
            modifier = Modifier.fillMaxWidth(),

            value = state.password,

            isError = state.passwordError != null,

            textStyle = TextStyle(
                color = Color.Black,
                fontSize = 16.sp
            ),

            placeholder = { if (state.password.isEmpty()) Text("Пароль") },

            colors = TextFieldDefaults.textFieldColors(
                textColor = Color.Gray,
                disabledTextColor = Color.Transparent,
                focusedIndicatorColor = Color.Transparent,
                unfocusedIndicatorColor = Color.Transparent,
                disabledIndicatorColor = Color.Transparent
            ),

            onValueChange = { currentPassword ->
                viewModel.onEvent(AuthFormEvent.PasswordOnChanged(currentPassword))
            },

            trailingIcon = {
                IconButton(
                    onClick = { passwordVisibility.value = !passwordVisibility.value }
                ) {
                    Icon(
                        painter = painterResource(
                            id = if (passwordVisibility.value) {
                                R.drawable.icon_vicibility
                            } else {
                                R.drawable.icon_visibility_off
                            }
                        ),
                        contentDescription = null
                    )
                }
            },

            visualTransformation = if (passwordVisibility.value) {
                VisualTransformation.None
            } else {
                PasswordVisualTransformation()
            },

            singleLine = true,

            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Password,
                imeAction = ImeAction.Next
            )
        )

        if (state.passwordError != null) {
            Text(
                text = state.passwordError,
                color = MaterialTheme.colors.error
            )
        }

        OutlinedTextField(
            modifier = Modifier.fillMaxWidth(),

            value = state.repeatedPassword,

            isError = state.repeatedPasswordError != null,

            textStyle = TextStyle(
                color = Color.Black,
                fontSize = 16.sp
            ),

            placeholder = { if (state.repeatedPassword.isEmpty()) Text("Повторить пароль") },

            colors = TextFieldDefaults.textFieldColors(
                textColor = Color.Gray,
                disabledTextColor = Color.Transparent,
                focusedIndicatorColor = Color.Transparent,
                unfocusedIndicatorColor = Color.Transparent,
                disabledIndicatorColor = Color.Transparent
            ),

            onValueChange = { currentRepeatedPassword ->
                viewModel.onEvent(AuthFormEvent.RepeatedPasswordOnChanged(currentRepeatedPassword))
            },

            trailingIcon = {
                IconButton(
                    onClick = {
                        repeatedPasswordVisibility.value = !repeatedPasswordVisibility.value
                    }
                ) {
                    Icon(
                        painter = painterResource(
                            id = if (passwordVisibility.value) {
                                R.drawable.icon_vicibility
                            } else {
                                R.drawable.icon_visibility_off
                            }
                        ),
                        contentDescription = null
                    )
                }
            },

            visualTransformation = if (repeatedPasswordVisibility.value) {
                VisualTransformation.None
            } else {
                PasswordVisualTransformation()
            },

            singleLine = true,

            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Password,
                imeAction = ImeAction.Done
            )
        )

        if (state.repeatedPasswordError != null) {
            Text(
                text = state.repeatedPasswordError,
                color = MaterialTheme.colors.error
            )
        }

        val context = LocalContext.current
        ClassicButton(
            "Продолжить",
            Modifier
                .fillMaxWidth()
                .padding(top = 10.dp)
                .height(45.dp),
            onClick = {
                coroutineScope.launch {
                    viewModel.onEvent(AuthFormEvent.RegistrationSubmit)
                    viewModel.validationEventChannel.collect { event ->
                        when (event) {
                            is ValidationEvent.Success -> {
                                navController.navigate(
                                    "${NavRoute.InputCodeScreen.route}/${ScenarioAuthEvent.REGISTER_ACCOUNT.name}"
                                )
                            }
                            is ValidationEvent.Failure -> {
                                Toast.makeText(
                                    context,
                                    "Попробуйте еще раз!",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                    }
                }
            }
        )
    }
}


