package com.example.rt.presentation.event

data class AuthorizationState(
    val email: String = "",
    val emailError: String? = null,
    val password: String = "",
    val passwordError: String? = null,
    val repeatedPassword: String = "",
    val repeatedPasswordError: String? = null,
    val confirmCode: List<Char>? = listOf('0','0','0','0','0','0')
)
