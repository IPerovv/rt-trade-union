package com.example.rt.presentation.main_menu_screens

import android.net.Uri
import android.widget.Toast
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Card
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.example.rt.navigation.NavRoute
import com.example.rt.presentation.event.CreateGroupEventEvent
import com.example.rt.presentation.event.ValidationEvent
import com.example.rt.presentation.page_elements.app_bars.SingleScreenTopBar
import com.example.rt.presentation.page_elements.buttons.ClassicButton
import com.example.rt.presentation.page_elements.content_parts.InputPhotoCreatePost
import com.example.rt.presentation.page_elements.content_parts.TextFieldCreatePost
import com.example.rt.presentation.theme.GrayBackgroundColor
import com.example.rt.presentation.theme.customAppTypography
import com.example.rt.presentation.viewmodel.CreateGroupEventsViewModel
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody

@Composable
fun CreateGroupEventsScreen(
    navController: NavController,
    viewModel: CreateGroupEventsViewModel = hiltViewModel()
) {
    val state = viewModel.state
    val coroutineScope = rememberCoroutineScope()
    val context = LocalContext.current
    val uriPhoto: MutableState<Uri?> = rememberSaveable { mutableStateOf(null) }

    val getContent = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.GetContent(),
        onResult = { uri ->
            uriPhoto.value = uri
            uri?.let {
                context.contentResolver.openInputStream(it)?.use { inputStream ->
                    val requestImage = RequestBody.create(
                        "image/*".toMediaTypeOrNull(), inputStream.readBytes()
                    )
                    val photoPart = MultipartBody.Part.createFormData(
                        "file",
                        "News.jpg",
                        requestImage
                    )
                    viewModel.onEvent(CreateGroupEventEvent.PhotoChanges(photoPart))
                }
            }
        }
    )

    Scaffold(
        modifier = Modifier.fillMaxSize(),
        content = {
            it
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .background(color = GrayBackgroundColor),
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                SingleScreenTopBar(
                    onClick = {},
                    text = "Создание событий",
                    navController = navController,
                    dotsBool = false
                )

                Card( //Карточка на которой расположен пост
                    shape = RoundedCornerShape(topStart = 14.dp, topEnd = 14.dp),
                    elevation = 5.dp,
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(top = 6.dp)
                        .verticalScroll(rememberScrollState())
                ) {
                    Column(
                        Modifier.fillMaxSize(),
                        verticalArrangement = Arrangement.SpaceBetween
                    ) {
                        Column {

                            CustomSpacer()

                            InputPhotoCreatePost(
                                onClick = {
                                    getContent.launch("image/*")
                                },
                                uriPhoto = uriPhoto.value
                            )

                            CustomSpacer()

                            TextFieldCreatePost(
                                state = state.title,
                                text = "Название",
                                onValueChange = { newText ->
                                    viewModel.onEvent(
                                        CreateGroupEventEvent.TitleChanges(
                                            newText.toString()
                                        )
                                    )
                                }
                            )

                            CustomSpacer()

                            TextFieldCreatePost(
                                state = state.description,
                                text = "Текст",
                                onValueChange = { newText ->
                                    viewModel.onEvent(
                                        CreateGroupEventEvent.DescriptionChanges(
                                            newText.toString()
                                        )
                                    )
                                }
                            )

                            CustomSpacer()

                            TextFieldCreatePost(
                                state = state.dateTime,
                                text = "Дата",
                                onValueChange = { newText ->
                                    viewModel.onEvent(
                                        CreateGroupEventEvent.DateTimeChanges(
                                            newText.toString()
                                        )
                                    )
                                }
                            )

                            CustomSpacer()

                            TextFieldCreatePost(
                                state = state.time,
                                text = "Время",
                                onValueChange = { time ->
                                    viewModel.onEvent(
                                        CreateGroupEventEvent.TimeChanges(
                                            time.toString()
                                        )
                                    )
                                }
                            )

                            CustomSpacer()

                            TextFieldCreatePost(
                                state = state.place,
                                text = "Место",
                                onValueChange = { newText ->
                                    viewModel.onEvent(
                                        CreateGroupEventEvent.PlaceChanges(
                                            newText.toString()
                                        )
                                    )
                                }
                            )

                        }

                        CustomSpacer()

                        ClassicButton(
                            "Опубликовать",
                            Modifier
                                .fillMaxWidth()
                                .padding(start = 15.dp, end = 15.dp, bottom = 20.dp)
                                .height(45.dp),
                            onClick = {
                                coroutineScope.launch {
                                    viewModel.onEvent(
                                        CreateGroupEventEvent.SubmitGroupEvent
                                    )
                                    viewModel.validationEventChannel.collect { event ->
                                        when (event) {
                                            is ValidationEvent.Success -> {
                                                navController.navigate(NavRoute.EventScreen.route)
                                            }
                                            is ValidationEvent.Failure -> {
                                                Toast.makeText(
                                                    context,
                                                    "Попробуйте ещё раз!",
                                                    Toast.LENGTH_LONG
                                                ).show()
                                            }
                                        }
                                    }
                                }
                            }
                        )
                    }
                }
            }
        }
    )
}

@Composable
private fun AdditionalSection() {
    Text(
        //Заголовок
        text = "Дополнительно:",
        style = customAppTypography.subtitle1,
        modifier = Modifier.padding(horizontal = 10.dp),
    )
}

@Composable
private fun CustomSpacer() {
    Spacer(
        modifier = Modifier
            .height(25.dp)
            .fillMaxWidth()
            .background(color = Color.White)
    )
}

