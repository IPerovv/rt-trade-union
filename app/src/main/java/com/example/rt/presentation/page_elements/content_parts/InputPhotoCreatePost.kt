package com.example.rt.presentation.page_elements.content_parts

import android.net.Uri
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import coil.compose.rememberImagePainter
import com.example.rt.R


@Composable
fun InputPhotoCreatePost(
    onClick: () -> Unit,
    uriPhoto: Uri? = null,
) {
    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.Center
    ) {
        Image(
            contentScale = ContentScale.Crop,
            modifier = Modifier
                .fillMaxWidth()
                .height(250.dp)
                .padding(horizontal = 16.dp)
                .clip(shape = RoundedCornerShape(14.dp))
                .clickable { onClick() },
            painter = if (uriPhoto == null) {
                painterResource(id = R.drawable.add_photo)
            } else {
                rememberImagePainter(uriPhoto)
            },
            contentDescription = "photo",
        )
    }
}