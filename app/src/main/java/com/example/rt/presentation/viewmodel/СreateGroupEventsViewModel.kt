package com.example.rt.presentation.viewmodel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.rt.data.remote.RtApi
import com.example.rt.data.remote.dto.GroupEventDto
import com.example.rt.domain.repository.PhotoRepository
import com.example.rt.presentation.event.CreateGroupEventEvent
import com.example.rt.presentation.event.ValidationEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import javax.inject.Inject

@HiltViewModel
class CreateGroupEventsViewModel @Inject constructor(
    private val rtApi: RtApi,
    private val photoRepository: PhotoRepository
) : ViewModel() {
    data class CreateGroupEventState(
        val title: String = "",
        val description: String = "",
        val dateTime: String = "",
        val time: String = "",
        val place: String = "",
        val photo: MultipartBody.Part? = null
    )

    var state by mutableStateOf(CreateGroupEventState())

    private val _validationEventChannel = Channel<ValidationEvent>()
    val validationEventChannel = _validationEventChannel.receiveAsFlow()

    fun onEvent(createGroupEventEvent: CreateGroupEventEvent) {
        when (createGroupEventEvent) {
            is CreateGroupEventEvent.TitleChanges -> {
                state = state.copy(title = createGroupEventEvent.title)
            }
            is CreateGroupEventEvent.DescriptionChanges -> {
                state = state.copy(description = createGroupEventEvent.description)
            }
            is CreateGroupEventEvent.PhotoChanges -> {
                state = state.copy(photo = createGroupEventEvent.photo)
            }
            is CreateGroupEventEvent.DateTimeChanges -> {
                state = state.copy(dateTime = createGroupEventEvent.dateTime)
            }
            is CreateGroupEventEvent.TimeChanges -> {
                state = state.copy(time = createGroupEventEvent.time)
            }
            is CreateGroupEventEvent.PlaceChanges -> {
                state = state.copy(place = createGroupEventEvent.place)
            }
            is CreateGroupEventEvent.SubmitGroupEvent -> {
                submitGroupEvent()
            }
        }
    }

    private fun submitGroupEvent() {
        viewModelScope.launch {
            try {
                val photoUrlResult = state.photo?.let {
                    photoRepository.uploadPhoto(it)
                }

                val result = rtApi.postGroupEvents(
                    GroupEventDto(
                        title = state.title,
                        description = state.description,
                        place = state.place,
                        photo = photoUrlResult?.fileName.toString(),
                        date = state.dateTime,
                        time = state.time,
                    )
                )

                _validationEventChannel.send(
                    if (result.isSuccessful) {
                        ValidationEvent.Success
                    } else {
                        ValidationEvent.Failure
                    }
                )
            } catch (exception: Exception) {
                _validationEventChannel.send(ValidationEvent.Failure)
            }
        }
    }
}