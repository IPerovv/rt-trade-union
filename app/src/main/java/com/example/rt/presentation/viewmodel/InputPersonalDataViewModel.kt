package com.example.rt.presentation.viewmodel

import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.rt.data.local.AccountStoreManager
import com.example.rt.data.remote.dto.PersonalDataDto
import com.example.rt.domain.repository.AuthenticationRepository
import com.example.rt.domain.repository.PhotoRepository
import com.example.rt.domain.use_case.validation.*
import com.example.rt.presentation.event.InputPersonalDataEvent
import com.example.rt.presentation.event.ValidationEvent
import com.example.rt.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import javax.inject.Inject

@HiltViewModel
class InputPersonalDataViewModel @Inject constructor(
    private val accountStoreManager: AccountStoreManager,
    private val authenticationRepository: AuthenticationRepository,
    private val photoRepository: PhotoRepository,
    private val validatePhoneUseCase: ValidatePhoneUseCase,
    private val validateNameUseCase: ValidateNameUseCase,
    private val validateSurnameUseCase: ValidateSurnameUseCase,
    private val validateLocationUseCase: ValidateLocationUseCase,
    private val validateBirthDayUseCase: ValidateBirthDayUseCase,
    private val validatePhotoUseCase: ValidatePhotoUseCase
) : ViewModel() {
    data class PersonalData(
        val firstName: String = "",
        val firstNameError: String? = null,
        val lastName: String = "",
        val lastNameError: String? = null,
        var birthDay: String = "",
        var birthDayError: String? = null,
        val phoneNumber: String = "",
        val phoneNumberError: String? = null,
        val location: String = "",
        val locationError: String? = null,
        val photoPart: MultipartBody.Part? = null,
        val photoError: String? = null,
    )

    var state by mutableStateOf(PersonalData())

    private val _validationChannel = Channel<ValidationEvent>()
    val validationChannel = _validationChannel.receiveAsFlow()

    fun onEvent(inputPersonalDataEvent: InputPersonalDataEvent) {
        when (inputPersonalDataEvent) {
            is InputPersonalDataEvent.PhotoOnChanged -> {
                state = state.copy(photoPart = inputPersonalDataEvent.photoPart)
            }
            is InputPersonalDataEvent.FirstNameOnChanged -> {
                state = state.copy(firstName = inputPersonalDataEvent.firstName)
            }
            is InputPersonalDataEvent.LastNameOnChanged -> {
                state = state.copy(lastName = inputPersonalDataEvent.lastName)
            }
            is InputPersonalDataEvent.BirthDayOnChanged -> {
                state = state.copy(birthDay = inputPersonalDataEvent.birthDay)
            }
            is InputPersonalDataEvent.PhoneNumberOnChanged -> {
                state = state.copy(phoneNumber = inputPersonalDataEvent.phoneNumber)
            }
            is InputPersonalDataEvent.LocationOnChanged -> {
                state = state.copy(location = inputPersonalDataEvent.location)
            }
            is InputPersonalDataEvent.Submit -> {
                submitData()
            }
        }
    }

    private fun submitData() {
        viewModelScope.launch {
            val photoUrlResult = state.photoPart?.let {
                photoRepository.uploadPhoto(it)
            }

            if (photoUrlResult != null) {
                Log.i("MyTag", photoUrlResult.fileName)
                accountStoreManager.savePhotoUrl(photoUrlResult.fileName)
            }

            val phoneNumber = validatePhoneUseCase.execute(state.phoneNumber)
            val firstName = validateNameUseCase.execute(state.firstName)
            val lastName = validateSurnameUseCase.execute(state.lastName)
            val location = validateLocationUseCase.execute(state.location)
            val photo = validatePhotoUseCase.execute(state.photoPart)
            val birthDay = validateBirthDayUseCase.execute(state.birthDay)

            val hasError = listOf(phoneNumber, firstName, lastName, location, birthDay,photo).any {!it.successful}

            state = state.copy(
                firstNameError = firstName.errorMessage,
                lastNameError =  lastName.errorMessage,
                locationError = location.errorMessage,
                phoneNumberError = phoneNumber.errorMessage,
                photoError = photo.errorMessage,
                birthDayError = birthDay.errorMessage
            )

            if (!hasError){
                authenticationRepository.updatePersonalData(
                    PersonalDataDto(
                        firstName = state.firstName,
                        lastName = state.lastName,
                        phoneNumber = state.phoneNumber,
                        location = state.location,
                        photo = photoUrlResult?.fileName
                    )

                ).collect { event ->
                    when (event) {
                        is Resource.Loading -> {}
                        is Resource.Success -> {
                            _validationChannel.send(ValidationEvent.Success)
                        }
                        is Resource.Error -> {
                            _validationChannel.send(ValidationEvent.Failure)
                        }
                    }
                }
            }
        }
    }
}