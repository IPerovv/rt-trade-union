package com.example.rt.presentation.main_menu_screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.example.rt.R
import com.example.rt.domain.model.User
import com.example.rt.presentation.page_elements.app_bars.SingleScreenTopBar
import com.example.rt.presentation.page_elements.cards.RatingCard
import com.example.rt.presentation.theme.GrayBackgroundColor
import com.example.rt.presentation.theme.PinkLight


@Composable
fun RatingScreen(
    navController: NavHostController
) {
    Scaffold(backgroundColor = GrayBackgroundColor,
        topBar = {
            SingleScreenTopBar(
                onClick = {},
                text = "Рейтинг",
                navController = navController,
                dotsBool = false
            )
        },
        modifier = Modifier.fillMaxSize(),
        content = {
            it
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .background(color = GrayBackgroundColor),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center,
            ) {
                Column() {
                    LazyColumn(
                        modifier = Modifier
                            .padding(top = 5.dp)
                            .fillMaxWidth(),
                    )
                    {
                        item { //Cюда локального пользователя
                            RatingCard(
                                user = User(
                                    photo = R.drawable.image_test_1.toString(),
                                    firstName = "Зинаида",
                                    lastName = "Врот",
                                    rating = 25
                                ),
                                ratingPlace = 102,
                                modifier = Modifier.background(PinkLight)
                            )
                        }
                        itemsIndexed(
                            listOf(
                                //Cюда остальные
                                User(
                                    photo = R.drawable.image_test_1.toString(),
                                    firstName = "Зинаида",
                                    lastName = "Врот",
                                    rating = 1000
                                ),
                                User(
                                    photo = R.drawable.image_test_1.toString(),
                                    firstName = "Зинаида",
                                    lastName = "Врот",
                                    rating = 1000
                                ),
                                User(
                                    photo = R.drawable.image_test_1.toString(),
                                    firstName = "Зинаида",
                                    lastName = "Врот",
                                    rating = 1000
                                ),
                                User(
                                    photo = R.drawable.image_test_1.toString(),
                                    firstName = "Зинаида",
                                    lastName = "Врот",
                                    rating = 1000
                                ),
                                User(
                                    photo = R.drawable.image_test_1.toString(),
                                    firstName = "Зинаида",
                                    lastName = "Врот",
                                    rating = 1000
                                ),
                                User(
                                    photo = R.drawable.image_test_1.toString(),
                                    firstName = "Зинаида",
                                    lastName = "Врот",
                                    rating = 1000
                                ),
                                User(
                                    photo = R.drawable.image_test_1.toString(),
                                    firstName = "Зинаида",
                                    lastName = "Врот",
                                    rating = 1000
                                ),
                                User(
                                    photo = R.drawable.image_test_1.toString(),
                                    firstName = "Зинаида",
                                    lastName = "Врот",
                                    rating = 1000
                                ),
                                User(
                                    photo = R.drawable.image_test_1.toString(),
                                    firstName = "Зинаида",
                                    lastName = "Врот",
                                    rating = 1000
                                ),
                                User(
                                    photo = R.drawable.image_test_1.toString(),
                                    firstName = "Зинаида",
                                    lastName = "Врот",
                                    rating = 1000
                                ),
                                User(
                                    photo = R.drawable.image_test_1.toString(),
                                    firstName = "Зинаида",
                                    lastName = "Врот",
                                    rating = 1000
                                ),
                                User(
                                    photo = R.drawable.image_test_1.toString(),
                                    firstName = "Зинаида",
                                    lastName = "Врот",
                                    rating = 1000
                                ),
                            )
                        ) { ratingPlace, item ->
                            RatingCard(
                                user = item,
                                ratingPlace = ratingPlace + 1,
                                modifier = Modifier.background(Color.White)
                            )
                        }
                    }
                }
            }
        }
    )
}