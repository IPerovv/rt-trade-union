package com.example.rt.presentation.viewmodel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.rt.data.remote.RtApi
import com.example.rt.data.remote.dto.NewsDto
import com.example.rt.domain.repository.PhotoRepository
import com.example.rt.presentation.event.CreateNewsEvent
import com.example.rt.presentation.event.ValidationEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import javax.inject.Inject

@HiltViewModel
class CreateNewsViewModel @Inject constructor(
    private val rtApi: RtApi,
    private val photoRepository: PhotoRepository,
) : ViewModel() {
    data class CreateNewsState(
        val title: String = "",
        val description: String = "",
        val photo: MultipartBody.Part? = null
    )

    var state by mutableStateOf(CreateNewsState())

    private val _validationEventChannel = Channel<ValidationEvent>()
    val validationEventChannel = _validationEventChannel.receiveAsFlow()

    fun onEvent(createNewsEvent: CreateNewsEvent) {
        when (createNewsEvent) {
            is CreateNewsEvent.TitleChanges -> {
                state = state.copy(title = createNewsEvent.title)
            }
            is CreateNewsEvent.DescriptionChanges -> {
                state = state.copy(description = createNewsEvent.description)
            }
            is CreateNewsEvent.PhotoChanges -> {
                state = state.copy(photo = createNewsEvent.photoPart)
            }
            is CreateNewsEvent.SubmitNews -> {
                submitNews()
            }
        }
    }

    private fun submitNews() {
        viewModelScope.launch {
            try {
                val photoUrlResult = state.photo?.let {
                    photoRepository.uploadPhoto(it)
                }

                val result = rtApi.postNews(
                    NewsDto(
                        title = state.title,
                        description = state.description,
                        photo = photoUrlResult?.fileName.toString(),
                        isLiked = false,
                        cntComments = 0,
                        cntLikes = 0
                    )
                )
                _validationEventChannel.send(
                    if (result.isSuccessful) {
                        ValidationEvent.Success
                    } else {
                        ValidationEvent.Failure
                    }
                )
            } catch (exception: Exception) {
                _validationEventChannel.send(ValidationEvent.Failure)
            }
        }
    }
}