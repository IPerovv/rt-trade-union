package com.example.rt.presentation.page_elements.content_parts

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

@Composable
fun DatetimePlaceBox(
    datetime: String,
    place: String,
    style: TextStyle
) { //Объединение всей информации
    Column(modifier = Modifier.padding(horizontal = 10.dp)) {
        //PlannedDate(datetime, style)   //Дата
        Spacer(modifier = Modifier.size(5.dp))
        //PlannedTime(datetime, style)   //Время
        Spacer(modifier = Modifier.size(5.dp))
        //Place(place, style)            //Место
    }
}

@Composable
private fun PlannedDate(datetime: String, style: TextStyle) { //Дата
    val dateTime = LocalDateTime.parse(datetime, DateTimeFormatter.ISO_OFFSET_DATE_TIME)
    val dateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy")
    val formattedDate = dateTime.format(dateFormatter)
    Text(
        text = formattedDate,
        style = style,
    )
}

@Composable
private fun PlannedTime(datetime: String, style: TextStyle) { //Время
    val dateTime = LocalDateTime.parse(datetime, DateTimeFormatter.ISO_OFFSET_DATE_TIME)
    val dateFormatter = DateTimeFormatter.ofPattern("HH:mm")
    val formattedTime = dateTime.format(dateFormatter)
    Text(
        text = formattedTime,
        style = style,
    )
}

@Composable
fun Place(place: String, style: TextStyle) {  //Место
    Text(
        text = place,
        style = style,
    )
}



