package com.example.rt.presentation.event

data class InputPersonalDateState(
    val firstName: String = "",
    val firstNameError: String? = null,
    val lastName: String = "",
    val lastNameError: String? = null,
    val phoneNumber: String = "",
    val phoneNumberError: String? = null,
    val location: String = "",
    val locationError: String? = null,
)
