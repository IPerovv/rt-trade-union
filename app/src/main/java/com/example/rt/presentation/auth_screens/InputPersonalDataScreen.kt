package com.example.rt.presentation.auth_screens


import android.app.DatePickerDialog
import android.net.Uri
import android.widget.DatePicker
import android.widget.Toast
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.compose.rememberImagePainter
import com.example.rt.R
import com.example.rt.navigation.NavRoute
import com.example.rt.presentation.event.InputPersonalDataEvent
import com.example.rt.presentation.event.ValidationEvent
import com.example.rt.presentation.page_elements.app_bars.AuthTopBar
import com.example.rt.presentation.page_elements.buttons.ClassicButton
import com.example.rt.presentation.theme.GrayBackgroundColor
import com.example.rt.presentation.viewmodel.InputPersonalDataViewModel
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.util.*

@Composable
fun InputPersonalDataScreen(navController: NavController) {
    val viewModel: InputPersonalDataViewModel = hiltViewModel()
    val state = viewModel.state
    val context = LocalContext.current
    val coroutineScope = rememberCoroutineScope()

    Scaffold(
        modifier = Modifier.fillMaxSize(),
        topBar = {
            AuthTopBar("Регистрация")
        },
        content = { innerPadding ->
            Image(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(innerPadding),
                painter = painterResource(id = R.drawable.background_auth),
                contentDescription = "Фон",
                contentScale = ContentScale.Crop
            )
            Box(
                modifier = Modifier
                    .padding(top = 80.dp)
                    .fillMaxSize()
            ) {
                Card(
                    modifier = Modifier
                        .padding(top = 80.dp)
                        .fillMaxSize(),
                    shape = RoundedCornerShape(
                        topStart = 20.dp,
                        topEnd = 20.dp
                    ),
                    backgroundColor = Color.White,
                    elevation = 5.dp
                ) {
                    Column(
                        modifier = Modifier
                            .padding(
                                top = 80.dp,
                                bottom = 30.dp,
                                start = 20.dp,
                                end = 20.dp,
                            )
                            .fillMaxSize(),
                        horizontalAlignment = Alignment.Start,
                        verticalArrangement = Arrangement.SpaceBetween
                    ) {
                        FirstNameField(viewModel)
                        LastNameField(viewModel)
                        BirthDayPickerField(viewModel)
                        PhoneNumberField(viewModel)
                        LocationField(viewModel)
                        ClassicButton(
                            "Продолжить",
                            Modifier
                                .fillMaxWidth()
                                .padding(
                                    start = 15.dp,
                                    end = 15.dp,
                                )
                                .padding(bottom = innerPadding.calculateBottomPadding())
                                .height(45.dp),
                            onClick = {
                                coroutineScope.launch {
                                    viewModel.onEvent(InputPersonalDataEvent.Submit)
                                    viewModel.validationChannel.collect { event ->
                                        when (event) {
                                            is ValidationEvent.Success -> {
                                                navController.navigate(NavRoute.ProfileScreen.route)
                                            }

                                            is ValidationEvent.Failure -> {
                                                Toast.makeText(
                                                    context,
                                                    "Попробуйте еще раз!",
                                                    Toast.LENGTH_SHORT
                                                ).show()
                                            }
                                        }
                                    }
                                }
                            }
                        )

                    }
                }

                val uriPhoto: MutableState<Uri?> = rememberSaveable { mutableStateOf(null) }
                val getContent = rememberLauncherForActivityResult(
                    contract = ActivityResultContracts.GetContent(),
                    onResult = { uri ->
                        uriPhoto.value = uri
                        uri?.let {
                            context.contentResolver.openInputStream(it)?.use { inputStream ->
                                val requestImage = RequestBody.create(
                                    "image/*".toMediaTypeOrNull(), inputStream.readBytes()
                                )
                                val photoPart = MultipartBody.Part.createFormData(
                                    "file",
                                    "MyPhoto.jpg",
                                    requestImage
                                )

                                viewModel.onEvent(InputPersonalDataEvent.PhotoOnChanged(photoPart))
                            }
                        }
                    }
                )

                Image(
                    modifier = Modifier
                        .clip(CircleShape)
                        .size(160.dp)
                        .border(
                            BorderStroke(
                                7.dp,
                                if (state.photoError != null) {
                                    MaterialTheme.colors.error
                                } else {
                                    GrayBackgroundColor
                                }
                            ),
                            CircleShape
                        )
                        .align(Alignment.TopCenter)
                        .clickable {
                            getContent.launch("image/*")
                        },
                    painter = if (uriPhoto.value == null) {
                        painterResource(id = R.drawable.add_circle_button)
                    } else {
                        rememberImagePainter(uriPhoto.value)
                    },
                    contentScale = ContentScale.Crop,
                    alignment = Alignment.Center,
                    contentDescription = "Загрузить фотографию",
                )

            }
        },
    )
}


@Composable
private fun FirstNameField(viewModel: InputPersonalDataViewModel) {
    val state = viewModel.state
    OutlinedTextField(
        modifier = Modifier
            .fillMaxWidth(),

        value = state.firstName,

        isError = state.firstNameError != null,

        textStyle = TextStyle(
            color = Color.Black,
            fontSize = 16.sp
        ),

        placeholder = { if (state.firstName.isEmpty()) Text("Имя") },

        colors = TextFieldDefaults.textFieldColors(
            textColor = Color.Gray,
            disabledTextColor = Color.Transparent,
            focusedIndicatorColor = Color.Transparent,
            unfocusedIndicatorColor = Color.Transparent,
            disabledIndicatorColor = Color.Transparent
        ),

        onValueChange = { firstName ->
            viewModel.onEvent(InputPersonalDataEvent.FirstNameOnChanged(firstName))
        },

        singleLine = true,

        keyboardOptions = KeyboardOptions(
            keyboardType = KeyboardType.Text,
            imeAction = ImeAction.Next
        ),
    )
    Text(
        text =
        (state.firstNameError ?: " "),
        color = MaterialTheme.colors.error
    )

}

@Composable
private fun LastNameField(viewModel: InputPersonalDataViewModel) {
    val state = viewModel.state
    OutlinedTextField(
        modifier = Modifier
            .fillMaxWidth(),

        value = state.lastName,

        isError = state.lastNameError != null,

        textStyle = TextStyle(
            color = Color.Black,
            fontSize = 16.sp
        ),

        placeholder = { if (state.lastName.isEmpty()) Text("Фамилия") },

        colors = TextFieldDefaults.textFieldColors(
            textColor = Color.Gray,
            disabledTextColor = Color.Transparent,
            focusedIndicatorColor = Color.Transparent,
            unfocusedIndicatorColor = Color.Transparent,
            disabledIndicatorColor = Color.Transparent
        ),

        onValueChange = { lastName ->
            viewModel.onEvent(InputPersonalDataEvent.LastNameOnChanged(lastName))
        },

        singleLine = true,

        keyboardOptions = KeyboardOptions(
            keyboardType = KeyboardType.Text,
            imeAction = ImeAction.Next
        )
    )
    Text(
        text =
        (state.lastNameError ?: " "),
        color = MaterialTheme.colors.error
    )
}

@Composable
private fun PhoneNumberField(viewModel: InputPersonalDataViewModel) {
    val state = viewModel.state
    OutlinedTextField(
        modifier = Modifier
            .fillMaxWidth(),

        value = state.phoneNumber,

        isError = state.phoneNumberError != null,

        textStyle = TextStyle(
            color = Color.Black,
            fontSize = 16.sp
        ),

        placeholder = { if (state.phoneNumber.isEmpty()) Text("Телефон") },

        colors = TextFieldDefaults.textFieldColors(
            textColor = Color.Gray,
            disabledTextColor = Color.Transparent,
            focusedIndicatorColor = Color.Transparent,
            unfocusedIndicatorColor = Color.Transparent,
            disabledIndicatorColor = Color.Transparent
        ),

        onValueChange = { phoneNumber ->
            viewModel.onEvent(InputPersonalDataEvent.PhoneNumberOnChanged(phoneNumber))
        },

        singleLine = true,

        keyboardOptions = KeyboardOptions(
            keyboardType = KeyboardType.Phone,
            imeAction = ImeAction.Next
        )
    )
    Text(
        text =
        (state.phoneNumberError ?: " "),
        color = MaterialTheme.colors.error
    )
}

@Composable
private fun LocationField(viewModel: InputPersonalDataViewModel) {
    val state = viewModel.state
    OutlinedTextField(
        modifier = Modifier
            .fillMaxWidth(),

        value = state.location,

        isError = state.locationError != null,

        textStyle = TextStyle(
            color = Color.Black,
            fontSize = 16.sp
        ),

        placeholder = { if (state.location.isEmpty()) Text("Место проживания") },

        colors = TextFieldDefaults.textFieldColors(
            textColor = Color.Gray,
            disabledTextColor = Color.Transparent,
            focusedIndicatorColor = Color.Transparent,
            unfocusedIndicatorColor = Color.Transparent,
            disabledIndicatorColor = Color.Transparent
        ),

        onValueChange = { location ->
            viewModel.onEvent(InputPersonalDataEvent.LocationOnChanged(location))
        },

        singleLine = true,

        keyboardOptions = KeyboardOptions(
            keyboardType = KeyboardType.Text,
            imeAction = ImeAction.Done
        )
    )
    Text(
        text =
        (state.locationError ?: " "),
        color = MaterialTheme.colors.error
    )
}


@Composable
private fun BirthDayPickerField(viewModel: InputPersonalDataViewModel) {
    val state = viewModel.state
    val mContext = LocalContext.current

    val mCalendar = Calendar.getInstance()
    val mYear = mCalendar.get(Calendar.YEAR)
    val mMonth = mCalendar.get(Calendar.MONTH)
    val mDay = mCalendar.get(Calendar.DAY_OF_MONTH)
    mCalendar.time = Date()

    val mDatePickerDialog = DatePickerDialog(
        mContext,
        { _: DatePicker, mYear: Int, mMonth: Int, mDayOfMonth: Int ->
            viewModel.onEvent(InputPersonalDataEvent.BirthDayOnChanged("$mDayOfMonth.${mMonth + 1}.$mYear"))
        }, mYear, mMonth, mDay
    )

    OutlinedTextField(
        modifier = Modifier
            .fillMaxWidth()
            .clickable {
                mDatePickerDialog.show()
            }
            .border(
                BorderStroke(
                    1.dp,
                    if (state.birthDayError != null) {
                        MaterialTheme.colors.error
                    } else {
                        Color.Transparent
                    }
                ),
            ),

        value = state.birthDay,

        enabled = false,

        textStyle = TextStyle(
            color = Color.Black,
            fontSize = 16.sp
        ),

        placeholder = {
            if (state.birthDay.isEmpty()) Text(
                "Дата рождения",
                color = Color.DarkGray
            )
        },

        colors = TextFieldDefaults.textFieldColors(
            textColor = Color.Gray,
            disabledTextColor = Color.Transparent,
            focusedIndicatorColor = Color.Transparent,
            unfocusedIndicatorColor = Color.Transparent,
            disabledIndicatorColor = Color.Transparent
        ),


        onValueChange = {},

        singleLine = true,

        keyboardOptions = KeyboardOptions(
            keyboardType = KeyboardType.Text,
            imeAction = ImeAction.Next
        )
    )
    Text(
        text =
        (state.birthDayError ?: " "),
        color = MaterialTheme.colors.error
    )
}