package com.example.rt.presentation.page_elements.cards

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.rt.R
import com.example.rt.domain.model.Document
import com.example.rt.navigation.NavRoute
import com.example.rt.presentation.theme.*

@Composable
fun JoinProfsProfileCard(navController: NavController? = null) {
    Card(
        //Карточка на которой расположена кнопка "Вступить в профсоюз"
        modifier = Modifier
            .fillMaxWidth()
            .padding(5.dp)
            .clip(RoundedCornerShape(14.dp)),
        shape = RoundedCornerShape(14.dp),
        elevation = 5.dp,
    ) {
        Column() {
            Spacer(modifier = Modifier.size(7.dp))
            TextProfs(navController)
            Spacer(modifier = Modifier.size(10.dp))
            JoinProfsIcon()
            Spacer(modifier = Modifier.size(13.dp))
            JoinProfsButton(navController)
            Spacer(modifier = Modifier.size(13.dp))
        }
    }
}

@Composable
private fun TextProfs(navController: NavController? = null) {
    Column(
        horizontalAlignment = Alignment.Start,
        modifier = Modifier.padding(start = 15.dp)
    ) {
        Text(text = "Профсоюз", style = customAppTypography.caption)
        /*Text(
            text = "Что это?",
            fontFamily = rostelecomFontFamily,
            fontWeight = FontWeight.Medium,
            fontSize = 14.sp,
            color = BlueLinkColor,
            modifier = Modifier
                .padding(end = 3.dp)
                .clickable {
                    navController.currentBackStackEntry
                        ?.arguments
                        ?.putParcelable(
                            "DOCUMENT", Document(
                                "Что такое профсоюз?",
                                "- Коллективная защита интересов через          \n" +
                                        "коллективный договор;\n" +
                                        "- Возможность участия в принятии решений   производственных \n" +
                                        "и социальных проблем коллектива,  получения информации \n" +
                                        "о положении дел на предприятии;\n" +
                                        "- Бесплатные юридические консультации;\n" +
                                        "- Защита в вопросах охраны труда \n" +
                                        "и техники безопасности;\n" +
                                        "- Возможность получения моральной поддержки и материальной помощи \n" +
                                        "из средств профсоюза;\n" +
                                        "- Возможность через свою организацию решения проблем в государственных \n" +
                                        "и вышестоящих профсоюзных органах;\n" +
                                        "- Возможность участия члена профсоюза \n" +
                                        "и его семьи в культурно-спортивных мероприятиях;\n",
                                "Зачем нужен профсоюз?",
                                R.drawable.image_test_5,
                            )
                        )
                    navController.navigate(NavRoute.SingleDocumentScreen.route)
                },
        )*/
    }
}

@Composable
private fun JoinProfsIcon() {
    Column(
        modifier = Modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Image(
            painterResource(id = R.drawable.icon_join_profs),
            contentDescription = "icon_join_profs",
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 82.dp),
            contentScale = ContentScale.FillWidth,
        )
    }
}

@Composable
private fun JoinProfsButton(navController: NavController? = null) {
    Row(
        Modifier
            .fillMaxWidth()
            .padding(horizontal = 30.dp),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center,
    ) {
        Row(
            Modifier
                .fillMaxWidth()
                .clip(RoundedCornerShape(12.dp))
                .background(color = Orange)
                .clickable {
                    navController?.navigate(NavRoute.FeedbackScreen.route)
                }
                .padding(
                    horizontal = 5.dp,
                    vertical = 7.dp
                ),
            horizontalArrangement = Arrangement.Center
        ) {
            Text(
                text = "Вступить в профсоюз",
                color = ClearWhite,
                style = MaterialTheme.typography.buttonPostsType,
                modifier = Modifier.padding(end = 3.dp)
            )
        }
    }
}
