package com.example.rt.presentation.theme

import androidx.compose.material.Typography
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import com.example.rt.R

val rostelecomFontFamily = FontFamily(
    Font(R.font.rostelecom_basis_bold, FontWeight.Bold),
    Font(R.font.rostelecom_basis_light, FontWeight.Light),
    Font(R.font.rostelecom_basis_medium, FontWeight.Medium),
    Font(R.font.rostelecom_basis_regular, FontWeight.Normal),
)

val customAppTypography = Typography(
    caption = TextStyle(
        fontFamily = rostelecomFontFamily,
        fontWeight = FontWeight.Medium,
        fontSize = 18.sp,
    ),
    body1 = TextStyle(
        fontFamily = rostelecomFontFamily,
        fontWeight = FontWeight.Normal,
        fontSize = 15.sp,
    ),
    subtitle1 = TextStyle(
        fontFamily = rostelecomFontFamily,
        fontWeight = FontWeight.Medium,
        fontSize = 16.sp,
    ),
)

val Typography.buttonPostsType: TextStyle
    @Composable
    get() {
        return TextStyle(
            fontFamily = rostelecomFontFamily,
            fontWeight = FontWeight.Medium,
            fontSize = 16.sp,
        )
    }

val Typography.PostedDateType: TextStyle
    @Composable
    get() {
        return TextStyle(
            fontFamily = rostelecomFontFamily,
            fontWeight = FontWeight.Normal,
            fontSize = 13.sp,
        )
    }

val Typography.GroupEventInfoType: TextStyle
    @Composable
    get() {
        return TextStyle(
            fontFamily = rostelecomFontFamily,
            fontWeight = FontWeight.Medium,
            fontSize = 17.sp,
        )
    }

val Typography.BlackButtonType: TextStyle
    @Composable
    get() {
        return TextStyle(
            fontFamily = rostelecomFontFamily,
            fontWeight = FontWeight.Medium,
            fontSize = 18.sp,
            color = Color.White
        )
    }

val Typography.RatingType: TextStyle
    @Composable
    get() {
        return TextStyle(
            fontFamily = rostelecomFontFamily,
            fontWeight = FontWeight.Normal,
            fontSize = 16.sp,
        )
    }


val Typography.CoinType: TextStyle
    @Composable
    get() {
        return TextStyle(
            fontFamily = rostelecomFontFamily,
            fontWeight = FontWeight.Medium,
            fontSize = 23.sp,
        )
    }
