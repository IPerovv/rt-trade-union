package com.example.rt.presentation.viewmodel

import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.rt.data.remote.RtApi
import com.example.rt.data.remote.dto.ApplicationFormDto
import com.example.rt.presentation.event.FeedBackEvent
import com.example.rt.presentation.event.FeedBackState
import com.example.rt.presentation.event.ValidationEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FeedBackViewModel @Inject constructor(
    private val rtApi: RtApi
) : ViewModel() {
    private val _validationEventChannel = Channel<ValidationEvent>()
    val validationEventChannel = _validationEventChannel.receiveAsFlow()

    var state by mutableStateOf(FeedBackState())

    fun onEvent(feedBackEvent: FeedBackEvent) {
        when (feedBackEvent) {
            is FeedBackEvent.TextChanges -> {
                state = state.copy(text = feedBackEvent.text)
                Log.i("text changes", feedBackEvent.text)
            }
            is FeedBackEvent.SubmitData -> {
                submitData()
            }
        }
    }

    private fun submitData() {
        viewModelScope.launch {
            try {
                val result = rtApi.postApplicationForm(
                    ApplicationFormDto(
                        topic = "Заявка в профсоюз",
                        text = state.text
                    )
                )
                _validationEventChannel.send(
                    if (result.isSuccessful) {
                        ValidationEvent.Success
                    } else {
                        ValidationEvent.Failure
                    }
                )
            } catch (exception: Exception) {
                Log.e("error", exception.message.toString())
                _validationEventChannel.send(ValidationEvent.Failure)
            }
        }
    }
}