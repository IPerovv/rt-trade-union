package com.example.rt.presentation.main_menu_screens

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.FloatingActionButton
import androidx.compose.material.Icon
import androidx.compose.material.Scaffold
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import com.example.rt.navigation.NavRoute
import com.example.rt.presentation.page_elements.app_bars.BottomNavBar
import com.example.rt.presentation.page_elements.app_bars.MainScreenTopAppBar
import com.example.rt.presentation.page_elements.cards.GroupEventsCard
import com.example.rt.presentation.theme.GrayBackgroundColor
import com.example.rt.presentation.theme.Orange
import com.example.rt.presentation.viewmodel.GroupEventViewModel

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun ListedGroupEventsScreen(
    navController: NavHostController,
    viewModel: GroupEventViewModel = hiltViewModel()
) {
    val state = viewModel.state
    Scaffold(
        backgroundColor = GrayBackgroundColor,
        topBar = {
            MainScreenTopAppBar("События")
        },
        bottomBar = {
            BottomNavBar(navController = navController)
        },
        floatingActionButton = {
            FloatingActionButton(
                onClick = { navController.navigate(NavRoute.CreateGroupEventScreen.route) },
                backgroundColor = Orange
            ) {
                Icon(Icons.Filled.Add, contentDescription = "AddGroupEvent")
            }
        },
        content = { innerPadding ->
            LazyColumn(
                modifier = Modifier
                    .padding(innerPadding)
                    .fillMaxWidth()
            ) {
                items(state.listGroupEvents.size) { index ->
                    val groupEvent = state.listGroupEvents[index]
                    GroupEventsCard(
                        item = groupEvent,
                        onClick = {
                            navController.currentBackStackEntry
                                ?.arguments
                                ?.putParcelable("GROUP EVENT", groupEvent)
                            navController.navigate(NavRoute.SingleGroupEventScreen.route)
                        }
                    )
                }
            }
        }
    )
}