package com.example.rt.presentation.single_screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Card
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.example.rt.domain.model.Document
import com.example.rt.presentation.page_elements.app_bars.SingleScreenTopBar
import com.example.rt.presentation.page_elements.content_parts.ContentDescription
import com.example.rt.presentation.page_elements.content_parts.ContentPictureOld
import com.example.rt.presentation.page_elements.content_parts.ContentSubtitle
import com.example.rt.presentation.page_elements.content_parts.ContentTitle
import com.example.rt.presentation.theme.GrayBackgroundColor
import com.example.rt.presentation.theme.customAppTypography


@Composable
fun SingleDocumentScreen(
    item: Document,
    navController: NavHostController
) {
    Scaffold(
        modifier = Modifier.fillMaxSize(),
        content = {
            it
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .background(color = GrayBackgroundColor),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center,
            ) {
                SingleScreenTopBar(
                    onClick = {},
                    text = "Документы",
                    navController = navController,
                    dotsBool = true
                )

                Card( //Карточка на которой расположен пост
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(top = 6.dp)
                        .verticalScroll(rememberScrollState()),
                    shape =RoundedCornerShape(topStart = 14.dp, topEnd = 14.dp),
                    elevation = 5.dp
                ) {
                    Box {
                        Column {
                            Spacer(modifier = Modifier.size(15.dp))
                            ContentTitle(item.title)                //Заголовок
                            Spacer(modifier = Modifier.size(17.dp))
                            ContentSubtitle(item.subtitle)          //Подзаголовок
                            Spacer(modifier = Modifier.size(3.dp))
                            ContentDescription(item.description)    //Текст поста
                            Spacer(modifier = Modifier.size(8.dp))
                            ContentPictureOld(photo = item.photo)   //Фото
                            Spacer(modifier = Modifier.size(9.dp))
                            //AdditionalSection()                     //Дополнительные файлы
                        }
                    }
                }
            }
        }
    )
}

@Composable
private fun AdditionalSection() {
    Text(
        //Заголовок
        text = "Дополнительно:",
        style = customAppTypography.subtitle1,
        modifier = Modifier.padding(horizontal = 10.dp),
    )
}