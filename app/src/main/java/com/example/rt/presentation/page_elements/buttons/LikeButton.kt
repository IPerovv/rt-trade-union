package com.example.rt.presentation.page_elements.buttons

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.example.rt.R
import com.example.rt.presentation.event.NewsEvent
import com.example.rt.presentation.page_elements.logic_parts.reduceNumerals
import com.example.rt.presentation.theme.DarkGrayTypeColor
import com.example.rt.presentation.theme.GrayBasicColor
import com.example.rt.presentation.theme.RedBackgroundThumbUpColor
import com.example.rt.presentation.theme.buttonPostsType
import com.example.rt.presentation.viewmodel.NewsViewModel

@Composable
fun LikeButton(
    viewModel: NewsViewModel,
    newsId: Int,
    likeBool: Boolean,
    likesCount: Long,
) {
    val isLiked = remember {
        mutableStateOf(likeBool)
    }

    val likesCount = remember {
        mutableStateOf(likesCount)
    }

    Row( //Like
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Start,
        modifier = Modifier
            .background(
                color = likeBackgroundColorSwitch(isLiked),
                shape = RoundedCornerShape(12.dp)
            )
            .clip(RoundedCornerShape(12.dp))
            .clickable {
                likesCount.value = if (isLiked.value) {
                    viewModel.onEvent(NewsEvent.UnLike(newsId))
                    likesCount.value.dec()
                } else {
                    viewModel.onEvent(NewsEvent.Like(newsId))
                    likesCount.value.inc()
                }

                isLiked.value = !isLiked.value
            }
            .padding(
                horizontal = 5.dp,
                vertical = 5.dp
            )
    ) {
        Image(
            painter = painterResource(id = likeIconColorSwitch(isLiked)),
            contentDescription = "anyLikeImage",
            modifier = Modifier
                .padding(horizontal = 5.dp)
                .size(26.dp)
        )
        Text(
            text = reduceNumerals(likesCount.value),
            color = likeCountColorSwitch(isLiked),
            style = MaterialTheme.typography.buttonPostsType,
            modifier = Modifier.padding(end = 3.dp)
        )
    }
}

private fun likeIconColorSwitch(likeBool: MutableState<Boolean>): Int {
    return if (!likeBool.value) {
        R.drawable.icon_like_not_clicked
    } else {
        R.drawable.icon_like_cliked
    }
}

private fun likeBackgroundColorSwitch(likeBool: MutableState<Boolean>): Color {
    return if (!likeBool.value) GrayBasicColor else RedBackgroundThumbUpColor
}

private fun likeCountColorSwitch(likeBool: MutableState<Boolean>): Color {
    return if (!likeBool.value) DarkGrayTypeColor else Color.White
}