package com.example.rt.presentation.event

enum class ScenarioAuthEvent {
    REGISTER_ACCOUNT,
    RESTORE_ACCOUNT_PASSWORD,
}